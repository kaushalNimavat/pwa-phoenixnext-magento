<?php

namespace Rokanthemes\BookCategory\Block;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\DataObject\IdentityInterface;

class GetBookCategory extends \Magento\Catalog\Block\Product\AbstractProduct {

	/**
     * Default toolbar block name
     *
     * @var string
     */
    protected $_defaultToolbarBlock = 'Magento\Catalog\Block\Product\ProductList\Toolbar';

    /**
     * Product Collection
     *
     * @var AbstractCollection
     */
    protected $_productCollection;

    /**
     * Catalog layer
     *
     * @var \Magento\Catalog\Model\Layer
     */
    protected $_catalogLayer;

    /**
     * @var \Magento\Framework\Data\Helper\PostHelper
     */
    protected $_postDataHelper;

    /**
     * @var \Magento\Framework\Url\Helper\Data
     */
    protected $urlHelper;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;
    protected $productCollectionFactory;
    protected $storeManager;
    protected $catalogConfig;
    protected $productVisibility;
    protected $scopeConfig;

    protected $categoryFactory;

    /**
     * @param Context $context
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\Url\Helper\Data $urlHelper,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
		\Magento\Catalog\Model\Product\Visibility $productVisibility,
		\Magento\Catalog\Model\CategoryFactory $categoryFactory,
        array $data = []
    ) {
        $this->_catalogLayer = $layerResolver->get();
        $this->_postDataHelper = $postDataHelper;
        $this->categoryRepository = $categoryRepository;
        $this->urlHelper = $urlHelper;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->storeManager = $context->getStoreManager();
        $this->catalogConfig = $context->getCatalogConfig();
        $this->productVisibility = $productVisibility;

        $this->categoryFactory = $categoryFactory;

        parent::__construct(
            $context,
            $data
        );
    }

	public function getProducts($cid)
    {
    	$storeId    = $this->storeManager->getStore()->getId();
        $products = $this->productCollectionFactory->create()->setStoreId($storeId);

        $mainCategory = $this->categoryRepository->get($cid, $storeId);
        $subcategories = $mainCategory->getChildrenCategories();
        $productArray = array();
        $gg = 0 ;
        if (count($subcategories) > 0)
        {
            foreach($subcategories as $subcategory)
            { 
                if($gg == 3){break;} $gg++; //show only 3 sub categories
                 //echo $subcategory->getName();
                $subcategoryId = $subcategory->getId();
                $products = $this->productCollectionFactory->create()->setStoreId($storeId);
                $products
                    ->addAttributeToSelect('*')
                    ->addUrlRewrite($subcategoryId)
                    ->setPageSize(10)  // show only 10 products
                    ->addCategoriesFilter(['in' => $subcategoryId])
                    ->setVisibility($this->productVisibility->getVisibleInCatalogIds());

                if(count($products) <= 0)
                    continue;

                if(!$qty = $this->getConfig('qty'))
                    $qty = 8;
                 $keys = array();
                if($products->getSize() > $qty)
                {
                    $prids = $products->getAllIds();

                    if(!$prids)
                        break;

                    if($qty > count($prids))
                        $qty = count($prids);

                    $keys = array_rand($prids, $qty);

                    $ids = array();

                    foreach($keys as $key)
                        $ids[] = $prids[$key];

                    $products->addIdFilter($ids);  
                }
                $products->load();

                $productArray[$subcategory->getName()] = array('url' => $subcategory->getUrl(),'products' => $products);
            }
        }
        else
        {
            $products = $this->productCollectionFactory->create()->setStoreId($storeId);
            $products
                ->addAttributeToSelect('*')
                ->addUrlRewrite($cid)
                ->setPageSize(10)  // show only 10 products
                ->addCategoriesFilter(['in' => $cid])
                ->setVisibility($this->productVisibility->getVisibleInCatalogIds());

            if(!$qty = $this->getConfig('qty'))
                $qty = 8;
             $keys = array();
            if($products->getSize() > $qty)
            {
                $prids = $products->getAllIds();

                if($prids)
                {
                    if($qty > count($prids))
                    $qty = count($prids);

                    $keys = array_rand($prids, $qty);

                    $ids = array();

                    foreach($keys as $key)
                        $ids[] = $prids[$key];

                    $products->addIdFilter($ids);  
                }
            }
            $products->load();

            $productArray[$mainCategory->getName()] = array('url' => $mainCategory->getUrl(),'products' => $products);
        }
        return $productArray;
    }

	public function getConfig($att)
	{
		$path = 'bookcategory/bookcategory_config/' . $att;
		return $this->_scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
	
	function cut_string_featuredproduct($string,$number){
		if(strlen($string) <= $number) {
			return $string;
		}
		else {	
			if(strpos($string," ",$number) > $number){
				$new_space = strpos($string," ",$number);
				$new_string = substr($string,0,$new_space)."..";
				return $new_string;
			}
			$new_string = substr($string,0,$number)."..";
			return $new_string;
		}
	}
	
	public function getAddToCartPostParams(\Magento\Catalog\Model\Product $product)
    {
        $url = $this->getAddToCartUrl($product);
        return [
            'action' => $url,
            'data' => [
                'product' => $product->getEntityId(),
                \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED =>
                    $this->urlHelper->getEncodedUrl($url),
            ]
        ];
    }
}
