<?php 
namespace Rokanthemes\CategoryProductTab\Block;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\DataObject\IdentityInterface;

class CategoryProduct extends \Magento\Catalog\Block\Product\AbstractProduct 
{
	/**
     * Default toolbar block name
     *
     * @var string
     */
    protected $_defaultToolbarBlock = 'Magento\Catalog\Block\Product\ProductList\Toolbar';

    /**
     * Product Collection
     *
     * @var AbstractCollection
     */
    protected $_productCollection;

    /**
     * Catalog layer
     *
     * @var \Magento\Catalog\Model\Layer
     */
    protected $_catalogLayer;

    /**
     * @var \Magento\Framework\Data\Helper\PostHelper
     */
    protected $_postDataHelper;

    /**
     * @var \Magento\Framework\Url\Helper\Data
     */
    protected $urlHelper;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;
    protected $productCollectionFactory;
    protected $storeManager;
    protected $catalogConfig;
    protected $productVisibility;
    protected $scopeConfig;

    /**
     * @param Context $context
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\Url\Helper\Data $urlHelper,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
		\Magento\Catalog\Model\Product\Visibility $productVisibility,
        array $data = []
    ) {
        $this->_catalogLayer = $layerResolver->get();
        $this->_postDataHelper = $postDataHelper;
        $this->categoryRepository = $categoryRepository;
        $this->urlHelper = $urlHelper;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->storeManager = $context->getStoreManager();
        $this->catalogConfig = $context->getCatalogConfig();
        $this->productVisibility = $productVisibility;
        parent::__construct(
            $context,
            $data
        );
    }
	public function getConfig($value=''){

	   $config =  $this->_scopeConfig->getValue('categoryproducttab/new_status/'.$value, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	   return $config; 
	 
	}
	protected function getCustomerGroupId()
	{
		$customerGroupId =   (int) $this->getRequest()->getParam('cid');
		if ($customerGroupId == null) {
			$customerGroupId = $this->httpContext->getValue(Context::CONTEXT_GROUP);
		}
		return $customerGroupId;
	}
		
	public function getFeaturedProduct() {
		$storeId    = $this->storeManager->getStore()->getId();
		$category = $this->categoryRepository->get($this->_storeManager->getStore()->getRootCategoryId());
		$products = $category->getProductCollection();
		$products = $this->productCollectionFactory->create()->setStoreId($storeId);
		$products->joinField(
            'position',
            'catalog_category_product',
            'position',
            'product_id=entity_id',
            'category_id=' . (int)$category->getId()
        );
		$products
            ->addAttributeToSelect($this->catalogConfig->getProductAttributes())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addUrlRewrite($category->getId())
            ->setVisibility($this->productVisibility->getVisibleInCatalogIds());
		$products->addAttributeToFilter('featured', 1);
        $products->setPageSize($this->getConfig('qty'))->setCurPage(1);
		$this->_eventManager->dispatch(
            'catalog_block_product_list_collection',
            ['collection' => $products]
        );
		return $products;
	}
				
	public function getSaleProduct() {
			
		$storeId    = $this->storeManager->getStore()->getId();
		$layer =  $this->_catalogLayer;
		$category = $this->categoryRepository->get($this->_storeManager->getStore()->getRootCategoryId());
		$products = $category->getProductCollection();
		$products = $this->productCollectionFactory->create()->setStoreId($storeId);
		$products->joinField(
            'position',
            'catalog_category_product',
            'position',
            'product_id=entity_id',
            'category_id=' . (int)$category->getId()
        );
		$products
            ->addAttributeToSelect($this->catalogConfig->getProductAttributes())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addUrlRewrite($category->getId())
            ->setVisibility($this->productVisibility->getVisibleInCatalogIds());
		$todayDate= date('Y-m-d', time());
		$products->addAttributeToFilter('special_to_date', array('date'=>true, 'from'=> $todayDate));
        $products->setPageSize($this->getConfig('qty'))->setCurPage(1);
		$this->_eventManager->dispatch(
            'catalog_block_product_list_collection',
            ['collection' => $products]
        );
		return $products;
	}

	public function getNewProduct() {
		$storeId    = $this->storeManager->getStore()->getId();
		$todayDate= date('Y-m-d', time());
		$category = $this->categoryRepository->get($this->_storeManager->getStore()->getRootCategoryId());
		$products = $category->getProductCollection();
		$products = $this->productCollectionFactory->create()->setStoreId($storeId);
		$products->joinField(
            'position',
            'catalog_category_product',
            'position',
            'product_id=entity_id',
            'category_id=' . (int)$category->getId()
        );
		$products
            ->addAttributeToSelect($this->catalogConfig->getProductAttributes())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addUrlRewrite($category->getId())
            ->setVisibility($this->productVisibility->getVisibleInCatalogIds())
			->addAttributeToFilter('news_from_date', array('date'=>true, 'to'=> $todayDate))
			->addAttributeToSort('news_from_date','desc');	
        $products->setPageSize($this->getConfig('qty'))->setCurPage(1);
		$this->_eventManager->dispatch(
            'catalog_block_product_list_collection',
            ['collection' => $products]
        );
		return $products;
	}
	public function getRandomProduct() {
		$storeId    = $this->storeManager->getStore()->getId();
		$todayDate= date('Y-m-d', time());
		$category = $this->categoryRepository->get($this->_storeManager->getStore()->getRootCategoryId());
		$products = $category->getProductCollection();
		$products = $this->productCollectionFactory->create()->setStoreId($storeId);
		$products->joinField(
            'position',
            'catalog_category_product',
            'position',
            'product_id=entity_id',
            'category_id=' . (int)$category->getId()
        );
		$products
            ->addAttributeToSelect($this->catalogConfig->getProductAttributes())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addUrlRewrite($category->getId())
            ->setVisibility($this->productVisibility->getVisibleInCatalogIds());	
		$ids = array_rand($products->getAllIds(), $this->getConfig('qty'));
		$products->addAttributeToFilter('entity_id', array('in'=>$ids));
        $products->setPageSize($this->getConfig('qty'))->setCurPage(1);
		$this->_eventManager->dispatch(
            'catalog_block_product_list_collection',
            ['collection' => $products]
        );
		return $products;
	}
			
	public function getTabContent() 
    {
		$productTabs = array();
        $class = 'active';
        
        $rootCategory = $this->categoryRepository->get($this->_storeManager->getStore()->getRootCategoryId());

        // Parent Categories
        $parentCategories = $rootCategory->getChildrenCategories();
        //echo "parentCategories count is : " . count($parentCategories) . "<br>";
        
        
        foreach ($parentCategories as $targetCat) 
        {
            //echo "parentCategories id is : " . $targetCat->getId() . "<br>";
            //echo "parentCategories name is : " . $targetCat->getName() . "<br>";
            //echo "parentCategories Url is : " . $targetCat->getUrl() . "<br>";

            $products = $this->getLayout()->createBlock('Rokanthemes\BookCategory\Block\GetBookCategory')->getProducts($targetCat->getId());

            $productTabs[] = array('id' => $targetCat->getId().'_product', 'name' => $targetCat->getName(), 'productInfo' => $products, 'class'=> $class .' ++'.count($products));
            $class = '';
        }

        /*$bestProducts = $this->getLayout()->createBlock('Rokanthemes\BestsellerProduct\Block\Bestseller')->getBestSeller();
        $productTabs[] = array('id' => 'tba_bestseller_product', 'name' => 'ขายดี', 'productInfo' => $bestProducts, 'class'=> $class);

        $mostProducts = $this->getLayout()->createBlock('Rokanthemes\MostviewedProduct\Block\Mostviewed')->getProducts();
        $productTabs[] = array('id' => 'tba_mostview_product', 'name' => 'นิยม', 'productInfo' => $mostProducts, 'class'=> $class);
        

		if($this->getConfig('showbook'))
		{
			$bookProducts = $this->getLayout()->createBlock('Rokanthemes\BookCategory\Block\GetBookCategory')->getProducts(3);//3
			$productTabs[] = array('id'=>'book_product', 'name' => $this->getConfig('bookname'), 'productInfo' => $bookProducts, 'class'=> $class);
			$class = '';
		}
		if($this->getConfig('showebook'))
		{
			$ebookProducts = $this->getLayout()->createBlock('Rokanthemes\BookCategory\Block\GetBookCategory')->getProducts(6);//6
			$productTabs[] = array('id'=>'ebook_product','name' => $this->getConfig('ebookname'), 'productInfo' =>  $ebookProducts, 'class'=> $class);
            $class = '';
		}
        if($this->getConfig('showmerchandise'))
        {
            $merchandiseProduct = $this->getLayout()->createBlock('Rokanthemes\BookCategory\Block\GetBookCategory')->getProducts(7);//7
            $productTabs[] = array('id'=>'merchandise_product','name' => $this->getConfig('merchandisename'), 'productInfo' =>  $merchandiseProduct, 'class'=> $class);
            $class = '';
        }
		if($this->getConfig('showexcusive'))
		{
			$excusiveProducts = $this->getLayout()->createBlock('Rokanthemes\BookCategory\Block\GetBookCategory')->getProducts(15);//15
			$productTabs[] = array('id'=> 'excusive_product','name' => $this->getConfig('excusivename'), 'productInfo' =>  $excusiveProducts, 'class'=> $class);
            $class = '';
		}*/
		return $productTabs;
	}
}