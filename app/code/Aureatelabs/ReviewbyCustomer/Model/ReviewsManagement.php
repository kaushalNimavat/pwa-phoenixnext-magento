<?php

/**
 * Copyright © 2021 All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\ReviewbyCustomer\Model;

class ReviewsManagement implements \Aureatelabs\ReviewbyCustomer\Api\ReviewsManagementInterface
{

    protected $_reviewCollection;

    public function __construct(
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollection,
        \Magento\Catalog\Model\ProductFactory $product,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->_reviewCollection = $reviewCollection;
        $this->product = $product;
        $this->_storeManager = $storeManager;
        $this->_resource = $resource;
    }

    /**
     * {@inheritdoc}
     */
    public function getReviews($customer_id)
    {
        $reviewdetailTable =  $this->_resource->getTableName('review_detail');
        $collection = $this->_reviewCollection->create()
            ->addCustomerFilter($customer_id)->addStatusFilter(
                \Magento\Review\Model\Review::STATUS_APPROVED
                )->setDateOrder(); //->addRateVotes(); for add rate votes on response
        $collection->getSelect()->joinInner(
            ['review_detail' => $reviewdetailTable],
            'main_table.review_id=review_detail.review_id',
            ['store_id']
        );
        $collection->getData();
        $reviewResponse = [];
        foreach ($collection as $review) {
            $reviewRow = [];
            $product = $this->product->create()->load($review->getEntityPkValue());
            $reviewRow["review_id"] = $review->getReviewId();
            $reviewRow["created_at"] = $review->getCreatedAt();
            $reviewRow["status_id"] = $review->getStatusId();
            $reviewRow["title"] = $review->getTitle();
            $reviewRow["detail_id"] = $review->getDetailId();
            $reviewRow["detail"] = $review->getDetail();
            $reviewRow["nickname"] = $review->getNickname();
            $reviewRow["entity_pk_value"] = $review->getEntityPkValue();
            $reviewRow["product_name"]=  $product->getName();
            $reviewRow["store_id"]=  $review->getStoreId();

            $reviewResponse[] = $reviewRow;
        }
        return $reviewResponse;
    }
}
