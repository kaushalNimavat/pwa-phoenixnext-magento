<?php

namespace Aureatelabs\OrderExport\Ui\Component\Columns;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Aureatelabs\OrderExport\Controller\Adminhtml\Export\File\Download;
use Aureatelabs\OrderExport\Controller\Adminhtml\Export\File\Delete;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class ExportGridActions extends Column
{
    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * ExportGridActions constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = 'actions';
                if (isset($item['title'])) {
                    $item[$name]['view'] = [
                        'href' => $this->urlBuilder->getUrl(
                            Download::URL,
                            ['_query' => ['filename' => $item['title']]]
                        ),
                        'label' => __('Download')
                    ];
                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl(
                            Delete::URL,
                            ['_query' => ['filename' => $item['title'], 'id' => $item['entity_id']]]
                        ),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete'),
                            'message' => __('Are you sure you wan\'t to delete a file?')
                        ],
                        'post' => true,
                    ];
                }
            }
        }

        return $dataSource;
    }
}
