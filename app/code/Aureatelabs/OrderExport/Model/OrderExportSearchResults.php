<?php

declare(strict_types=1);

namespace Aureatelabs\OrderExport\Model;

use Aureatelabs\OrderExport\Api\Data\OrderExportSearchResultsInterface;
use Magento\Framework\Api\SearchResults;

class OrderExportSearchResults extends SearchResults implements OrderExportSearchResultsInterface
{
}
