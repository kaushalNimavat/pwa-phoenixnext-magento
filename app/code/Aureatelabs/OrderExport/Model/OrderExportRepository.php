<?php

namespace Aureatelabs\OrderExport\Model;

use Aureatelabs\OrderExport\Api\Data;
use Aureatelabs\OrderExport\Api\Data\OrderExportInterface;
use Aureatelabs\OrderExport\Api\Data\OrderExportSearchResultsInterface;
use Aureatelabs\OrderExport\Api\OrderExportRepositoryInterface;
use Aureatelabs\OrderExport\Model\ResourceModel\OrderExport as ResourceOrderExport;
use Aureatelabs\OrderExport\Model\ResourceModel\OrderExport\Collection;
use Aureatelabs\OrderExport\Model\ResourceModel\OrderExport\CollectionFactory as OrderExportCollectionFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class OrderExportRepository implements OrderExportRepositoryInterface
{
    /**
     * @var ResourceOrderExport
     */
    protected $resource;

    /**
     * @var OrderExportFactory
     */
    protected $orderExportFactory;

    /**
     * @var OrderExportCollectionFactory
     */
    protected $orderExportCollectionFactory;

    /**
     * @param ResourceOrderExport $resource
     * @param OrderExportFactory $orderExportFactory
     * @param OrderExportCollectionFactory $orderExportCollectionFactory
     */
    public function __construct(
        ResourceOrderExport $resource,
        OrderExportFactory $orderExportFactory,
        OrderExportCollectionFactory $orderExportCollectionFactory
    ) {
        $this->resource = $resource;
        $this->orderExportFactory = $orderExportFactory;
        $this->orderExportCollectionFactory = $orderExportCollectionFactory;
    }

    /**
     * @param string $entityId
     * @return OrderExportInterface
     * @throws NoSuchEntityException
     */
    public function getById(string $entityId): OrderExportInterface
    {
        $orderExport = $this->orderExportFactory->create();
        $this->resource->load($orderExport, $entityId);
        if (!$orderExport->getId()) {
            throw new NoSuchEntityException(__('The Order CSV with the "%1" ID doesn\'t exist.', $entityId));
        }
        return $orderExport;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return OrderExportSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): OrderExportSearchResultsInterface
    {
        /** @var Collection $collection */
        $collection = $this->orderExportCollectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var Data\OrderExportSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @param OrderExportInterface $orderExport
     * @return OrderExportInterface
     * @throws CouldNotSaveException
     */
    public function save(OrderExportInterface $orderExport): OrderExportInterface
    {
        try {
            $this->resource->save($orderExport);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $orderExport;
    }

    /**
     * @param OrderExportInterface $orderExport
     * @return bool
     */
    public function delete(OrderExportInterface $orderExport): bool
    {
        try {
            $this->resource->delete($orderExport);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param string $entityId
     * @return bool
     * @throws NoSuchEntityException
     */
    public function deleteById(string $entityId): bool
    {
        return $this->delete($this->getById($entityId));
    }
}
