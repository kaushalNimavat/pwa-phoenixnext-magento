<?php

namespace Aureatelabs\OrderExport\Model\ResourceModel\OrderExport;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Aureatelabs\OrderExport\Model\OrderExport::class,
            \Aureatelabs\OrderExport\Model\ResourceModel\OrderExport::class
        );
    }
}
