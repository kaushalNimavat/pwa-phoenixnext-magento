<?php

namespace Aureatelabs\OrderExport\Model\OrderExport;

use Aureatelabs\OrderExport\Api\Data\OrderExportInterface;
use Aureatelabs\OrderExport\Api\Data\OrderExportInterfaceFactory;
use Aureatelabs\OrderExport\Model\OrderExportRepository;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\WriteInterface;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Mirasvit\Rewards\Helper\Purchase;
use TBA\LOBOT\Controller\LobotHelper\LobotOrderTsvBuilder;

class CsvConsumer
{
    /**
     * @const Export path or sales CSV
     */
    const CSV_EXPORT_PATH = 'sales';

    /**
     * @const Sales data CSV data
     */
    const DATA_FILE_NAME = 'sales_order_report';

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var LobotOrderTsvBuilder
     */
    private $lobotOrderTsvBuilder;

    /**
     * @var CollectionFactory
     */
    private $orderCollectionFactory;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var File
     */
    private $fileDriver;

    /**
     * @var WriteInterface
     */
    private $directory;

    /**
     * @var array
     */
    private $salesCsvHeader = [
        'Sold to','Delivery date','Tracking number','Order number','Barcode or ISBN','Product name','Original price',
        'Price','Qty','Subtotal','Tax Amount','Tax Percent','Discount Amount','Row Total','Shipping',
        'Shipping before Tax','Tax of Shipping','Total Tax','JJJPayment method'
    ];

    /**
     * @var Purchase
     */
    private $rewardsPurchase;

    /**
     * @var OrderExportInterfaceFactory
     */
    private $orderExportInterfaceFactory;

    /**
     * @var OrderExportRepository
     */
    private $orderExportRepository;

    /**
     * @param Json $json
     * @param \Psr\Log\LoggerInterface $logger
     * @param CollectionFactory $orderCollectionFactory
     * @param Purchase $rewardsPurchase
     * @param Filesystem $filesystem
     * @param File $fileDriver
     * @param DirectoryList $directoryList
     * @param OrderExportInterfaceFactory $orderExportInterfaceFactory
     * @param OrderExportRepository $orderExportRepository
     * @throws FileSystemException
     */
    public function __construct(
        Json $json,
        \Psr\Log\LoggerInterface $logger,
        CollectionFactory $orderCollectionFactory,
        Purchase $rewardsPurchase,
        Filesystem $filesystem,
        File $fileDriver,
        DirectoryList $directoryList,
        OrderExportInterfaceFactory $orderExportInterfaceFactory,
        OrderExportRepository $orderExportRepository
    ) {
        $this->logger = $logger;
        $this->json = $json;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->rewardsPurchase = $rewardsPurchase;
        $this->directoryList = $directoryList;
        $this->fileDriver = $fileDriver;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->orderExportInterfaceFactory = $orderExportInterfaceFactory;
        $this->orderExportRepository = $orderExportRepository;
    }

    /**
     * @param $orders
     */
    public function process($orders)
    {
        $this->logger->critical('Consumer Started');
        try {
            $orderIds = $this->json->unserialize($orders);
            $this->execute($orderIds);
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * @param $orderIds
     * @throws FileSystemException
     */
    private function execute($orderIds)
    {
        $orderCollection = $this->orderCollectionFactory->create()
            ->addFieldToFilter('entity_id', ['in' => $orderIds]);
        if ($orderCollection->getSize()) {
            $orderData = $this->generateDataFromOrderCollection($orderCollection);
            if (!empty($orderData)) {
                $fileName = $this->createSalesDataCsv($orderData);
                /** @var OrderExportInterface $orderExport */
                $orderExport = $this->orderExportInterfaceFactory->create();
                $orderExport->setTitle($fileName);
                try {
                    $this->orderExportRepository->save($orderExport);
                } catch (\Magento\Framework\Exception\CouldNotSaveException $e) {
                    $this->logger->critical($e->getMessage());
                }
            }
        }
    }

    /**
     * @param $orderCollection
     * @return array
     */
    private function generateDataFromOrderCollection($orderCollection):array
    {
        $orderData = [];
        foreach ($orderCollection as $order) {
            $orderListId = $order->getEntityId();
            $purchase = $this->rewardsPurchase->getByOrder($order);
            $sum = $purchase->getSpendPoints();
            $productItems = $order->getAllItems();
            // deliveryCost
            $shipping = $this->decimalData($order->getShippingAmount() / count($productItems), 'float');
            foreach ($productItems as $product) {
                $productData = $product->getProduct();

                $emailCustomer = $order->getCustomerEmail();
                $shippingDate = $order->getData('shippingDate');
                $trackingNumber = $order->getData('trackingNumber');
                $orderId = $order->getIncrementId();
                $barcode = $productData->getData('barcode');
                $productName = $productData->getName();
                $originalPrice = $this->decimalData($productData->getPrice(), 'float');
                $qty = $this->decimalData($product->getData('qty_ordered'), 'number');
                $price = $this->decimalData($product->getRowTotalInclTax() / $qty, 'float');
                $subtotal = $this->decimalData($product->getRowTotalInclTax(), 'float');
                $subtotalInclTax = $order->getSubtotalInclTax();
                $taxAmount = $this->decimalData($product->getTaxAmount(), 'float');
                $discount = $orderListId > 4700 ?
                    (abs($order->getDiscountAmount()) + $sum) : abs($order->getDiscountAmount());
                $discountAmount = $this->decimalData($discount, 'float');

                $discountAmountCalc = ($subtotalInclTax > 0) ? (((float)$product->getRowTotalInclTax() / (float)$subtotalInclTax) * (float)$discount) : 0;
                $discountAmountTotal = $this->decimalData((float)$discountAmountCalc, 'float');

                $vat = $subtotal != 0 ? round(((float)$taxAmount / (float)$subtotal) * 100) : 0;
                if ($vat > 0) {
                    $vat = 7;
                }

                $rowTotal = floatval($product->getRowTotalInclTax()) - floatval($discountAmountCalc);
                $rowTotal = number_format($rowTotal, 2);

                $baseRowTotal = $order->getBaseRowTotal();

                $shippingBeforeTax = (float)$shipping / 1.07;
                $shippingBeforeTaxTotal = $this->decimalData($shippingBeforeTax, 'float');

                $taxOfShipping = (float)$shipping - (float)$shippingBeforeTaxTotal;
                $taxOfShippingTotal = $this->decimalData($taxOfShipping, 'float');

                $totalTax = $this->decimalData((float)$taxAmount + (float)$taxOfShippingTotal, 'float');

                $payment = $order->getPayment();
                $method = $payment->getMethodInstance();
                $paymentMethod = $method->getTitle();

                $orderData[] = [
                    $emailCustomer,
                    $shippingDate,
                    $trackingNumber,
                    $orderId,
                    $barcode,
                    $productName,
                    $originalPrice,
                    $price,
                    $qty,
                    $subtotal,
                    $taxAmount,
                    $vat,
                    $discountAmountTotal,
                    $rowTotal,
                    $shipping,
                    $shippingBeforeTaxTotal,
                    $taxOfShippingTotal,
                    $totalTax,
                    $paymentMethod
                ];
            }
        }
        return $orderData;
    }

    /**
     * @param array $data
     * @return string
     * @throws FileSystemException
     */
    private function createSalesDataCsv(array $data = []): string
    {
        $fileName = self::DATA_FILE_NAME . '_' . date('Ymd_His') . ".csv";
        $filepath = self::CSV_EXPORT_PATH . '/' . $fileName;
        $this->logger->debug('Begin:: File is generating with name: '. $filepath);
        $this->directory->create(self::CSV_EXPORT_PATH);
        $stream = $this->directory->openFile($filepath, 'w+');
        $stream->lock();

        /** Added the header row in CSV file */
        $stream->writeCsv($this->salesCsvHeader);

        /** Added the sales data in CSV file */
        foreach ($data as $dataRow) {
            $stream->writeCsv($dataRow);
        }
        $this->logger->debug('End:: File is generating with name: '. $filepath);
        return $fileName;
    }

    /**
     * @param $data
     * @param $format
     * @return string
     */
    private function decimalData($data, $format): string
    {
        return $format == 'number' ? number_format($data) : number_format($data,2);
    }
}
