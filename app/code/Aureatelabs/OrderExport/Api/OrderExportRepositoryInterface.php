<?php

namespace Aureatelabs\OrderExport\Api;

use Aureatelabs\OrderExport\Api\Data\OrderExportInterface;
use Aureatelabs\OrderExport\Api\Data\OrderExportSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface OrderExportRepositoryInterface
{
    /**
     * Save orderExport.
     *
     * @param OrderExportInterface $orderExport
     * @return OrderExportInterface
     * @throws LocalizedException
     */
    public function save(Data\OrderExportInterface $orderExport): OrderExportInterface;

    /**
     * Retrieve orderExport.
     *
     * @param string $entityId
     * @return OrderExportInterface
     * @throws LocalizedException
     */
    public function getById(string $entityId): OrderExportInterface;

    /**
     * Retrieve orderExport matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return OrderExportSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): OrderExportSearchResultsInterface;

    /**
     * Delete orderExport.
     *
     * @param OrderExportInterface $orderExport
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(Data\OrderExportInterface $orderExport): bool;

    /**
     * Delete orderExport by ID.
     *
     * @param string $entityId
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById(string $entityId): bool;
}
