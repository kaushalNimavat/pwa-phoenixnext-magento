<?php

namespace Aureatelabs\OrderExport\Api\Data;

interface OrderExportInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ENTITY_ID      = 'entity_id';
    const TITLE         = 'title';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME   = 'update_time';
    /**#@-*/

    /**
     * @return int|null
     */
    public function getId();

    /**
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime(): ?string;

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime(): ?string;

    /**
     * Set ID
     *
     * @param $id
     * @return OrderExportInterface
     */
    public function setId($id): OrderExportInterface;

    /**
     * Set title
     *
     * @param string $title
     * @return OrderExportInterface
     */
    public function setTitle(string $title): OrderExportInterface;

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return OrderExportInterface
     */
    public function setCreationTime(string $creationTime): OrderExportInterface;

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return OrderExportInterface
     */
    public function setUpdateTime(string $updateTime): OrderExportInterface;
}
