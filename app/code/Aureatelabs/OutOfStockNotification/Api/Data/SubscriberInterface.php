<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_OutOfStockNotification
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\OutOfStockNotification\Api\Data;

interface SubscriberInterface {

    const SUBSCRIBE_ID = 'alert_stock_id';
    const PRODUCT_ID = 'product_id';
    const CUSTOMER_EMAIL = 'customer_id'; 
    const IS_SENT = 'status';
    const CREATED_AT = 'add_date';
    const UPDATE_AT = 'send_date';

    public function getAlertStockId();

    public function setAlertStockId($alertStockId);

    public function getProductId();

    public function setProductId($productId);

    public function getCustomerId();

    public function setCustomerId($customerId);

    public function getStatus();

    public function setStatus($status);

    public function getAddDate();

    public function setAddDate($addDate);

    public function getSendDate();

    public function setSendDate($sendDate);

}