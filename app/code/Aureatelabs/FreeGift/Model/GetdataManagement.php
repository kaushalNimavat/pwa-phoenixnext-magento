<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\FreeGift\Model;

use Mageplaza\FreeGifts\Model\Source\Apply;

class GetdataManagement implements \Aureatelabs\FreeGift\Api\GetdataManagementInterface
{

    /**
     * @var MaskedQuoteIdToQuoteIdInterface
     */
    private $maskedQuoteIdToQuoteId;

    /**
     * @param ProductRepository $productRepository
     * @param Session $customerSession
     * @param StoreManagerInterface $storeManager
     * @param Rule $helperRule
     * @param QuoteFactory $quoteFactory
     * @param MaskedQuoteIdToQuoteIdInterface $maskedQuoteIdToQuoteId
     * @param QuoteIdMaskFactory $quoteIdMask
     */

    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Mageplaza\FreeGifts\Helper\Rule $helperRule,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Quote\Model\MaskedQuoteIdToQuoteIdInterface $maskedQuoteIdToQuoteId,
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMask
    ){
        $this->_productRepository = $productRepository;
        $this->customerSession    = $customerSession;
        $this->_storeManager = $storeManager;
        $this->_helperRule        = $helperRule;
        $this->_quoteFactory      = $quoteFactory;
        $this->maskedQuoteIdToQuoteId       = $maskedQuoteIdToQuoteId;
        $this->_quoteIdMask       = $quoteIdMask;
    }
    /**
     * {@inheritdoc}
     */
    public function getGetdata($quote_id)
    {
        $cartId = $quote_id;
        $quoteFactory = $this->_quoteFactory->create();
        if(!is_numeric($cartId)){
            $quote_id_new = $this->_quoteIdMask->create()->load($cartId, 'masked_id');
            $quote = $quoteFactory->load($quote_id_new->getQuoteId());
        }else{
            $quote = $quoteFactory->load($cartId);
        }
        $items = $quote->getAllVisibleItems();
        if(!empty($items)){
            foreach($items as $item) {
                $product_id = $item->getProductId();
                $product = $this->_productRepository->getById($product_id);
                
                $response = $this->_helperRule->setQuote($quote)
                            ->setApply(Apply::CART)
                            ->setProduct($product)
                            ->getValidatedRules();
                
            }
            return $response;
        }else{
            return;
        }
    }
}
