<?php
namespace Aureatelabs\FreeGift\Plugin;

class GiftImageUrl
{
    /**
     * @param ProductModel $gift
     *
     * @return string
     * @throws NoSuchEntityException
     */
    public function afterGetGiftImage(\Mageplaza\FreeGifts\Helper\Gift $subject, $result, $gift)
    {
        return $gift->getImage();
    }
}
