<?php

namespace Aureatelabs\FreeGift\Plugin;

use Mageplaza\FreeGifts\Helper\Rule as HelperRule;
use Mageplaza\FreeGifts\Model\Gift\Item as GiftItem;

class RemoveCartApi
{

    /**
     * @var HelperRule
     */
    protected $_helperRule;

    /**
     * @var GiftItem
     */
    protected $_giftItem;

    private $quoteItemFactory;
    private $itemResourceModel;

    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        HelperRule $helperRule,
        GiftItem $giftItem
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->_helperRule      = $helperRule;
        $this->_giftItem        = $giftItem;
    }

    /**
     * @inheritdoc
     */
    public function afterDeleteById(\Magento\Quote\Api\CartItemRepositoryInterface $subject, $result, $cartId, $itemId)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);
        $quoteItems = $quote->getAllVisibleItems();
        $validRuleIds = array_keys($this->_helperRule->setExtraData(false)->getAllValidRules());
        $saveQuote    = false;
        foreach ($quoteItems as $quoteItem) {
            $itemRuleId = (int) $quoteItem->getDataByKey(HelperRule::QUOTE_RULE_ID);
            if ($itemRuleId && !in_array($itemRuleId, $validRuleIds, true)) {
                $this->_giftItem->removeAndDelete($quoteItem);
                $saveQuote = true;
            }
        }
        if ($saveQuote) {
            $quote->save();
            $quote->setTotalsCollectedFlag(false);
            $quote->collectTotals();
        }

        return true;
    }
}
