<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Banner
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Banner\Model\Indexer\Action;

use Aureatelabs\Banner\Model\ImageUploader;
use Aureatelabs\Banner\Model\ResourceModel\Banner as BannerResource;
use Divante\VsbridgeIndexerCore\Indexer\RebuildActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Banner
 * @package Aureatelabs\Banner\Model\Indexer\Action
 */
class Banner implements RebuildActionInterface
{
    /**
     * @var BannerResource
     */
    private $resourceModel;

    /**
     * @var ImageUploader
     */
    protected $imageUploader;

    /**
     * Banner constructor.
     * @param BannerResource $bannerResource
     * @param ImageUploader $imageUploader
     */
    public function __construct(
        BannerResource $bannerResource,
        ImageUploader $imageUploader
    ) {
        $this->resourceModel = $bannerResource;
        $this->imageUploader = $imageUploader;
    }

    /**
     * @param int $storeId
     * @param array $ids
     * @return \Traversable
     * @throws NoSuchEntityException
     * @throws \Exception
     */
    public function rebuild(int $storeId, array $ids): \Traversable
    {
        $lastBannerId = 0;
        do {
            $banners = $this->resourceModel->loadBanners($storeId, $ids, $lastBannerId);
            foreach ($banners as $banner) {
                $lastBannerId = $banner['entity_id'];
                $banner['id'] = $banner['entity_id'];
                $banner['image'] = $this->imageUploader->getFileUrl($banner['image']);
                $banner['active'] = (bool)$banner['is_active'];
                $banner['sort_order'] = (int)$banner['sort_order'];

                unset($banner['is_active']);
                yield $lastBannerId => $banner;
            }

        } while (!empty($banners));
    }
}
