<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\CustomerApi\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface CustomersRepositoryInterface
{

    /**
     * Save Customers
     * @param \Aureatelabs\CustomerApi\Api\Data\CustomersInterface $customers
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Aureatelabs\CustomerApi\Api\Data\CustomersInterface $customers
    );

    /**
     * Retrieve Customers matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Customers
     * @param \Aureatelabs\CustomerApi\Api\Data\CustomersInterface $customers
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Aureatelabs\CustomerApi\Api\Data\CustomersInterface $customers
    );

    /**
     * Delete Customers by ID
     * @param string $customersId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($customersId);

}

