<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\CustomerApi\Api\Data;

interface CustomersInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const CUSTOMERS_ID = 'customers_id';
    const NAME = 'name';
    const EMAIL = 'email';
    const NUMBER = 'number';
    const TOKEN = 'token';
    const BRAND = 'brand';
    const CREATED = 'created';
    const UPDATED = 'updated';

    /**
     * Get customers_id
     * @return string|null
     */
    public function getCustomersId();

    /**
     * Set customers_id
     * @param string $customersId
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setCustomersId($customersId);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setName($name);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Aureatelabs\CustomerApi\Api\Data\CustomersExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Aureatelabs\CustomerApi\Api\Data\CustomersExtensionInterface $extensionAttributes
    );

    /**
     * Get email
     * @return string|null
     */
    public function getEmail();

    /**
     * Set email
     * @param string $email
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setEmail($email);

    /**
     * Get token
     * @return string|null
     */
    public function getToken();

    /**
     * Set token
     * @param string $token
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setToken($token);

    /**
     * Get created
     * @return string|null
     */
    public function getCreated();

    /**
     * Set created
     * @param string $created
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setCreated($created);

    /**
     * Get updated
     * @return string|null
     */
    public function getUpdated();

    /**
     * Set updated
     * @param string $updated
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setUpdated($updated);

    /**
     * Get brand
     * @return string|null
     */
    public function getBrand();

    /**
     * Set brand
     * @param string $brand
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setBrand($brand);

    /**
     * Get number
     * @return string|null
     */
    public function getNumber();

    /**
     * Set number
     * @param string $number
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setNumber($number);
}

