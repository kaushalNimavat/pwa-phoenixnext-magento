<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\CustomerApi\Model\Data;

use Aureatelabs\CustomerApi\Api\Data\CustomersInterface;

class Customers extends \Magento\Framework\Api\AbstractExtensibleObject implements CustomersInterface
{

    /**
     * Get customers_id
     * @return string|null
     */
    public function getCustomersId()
    {
        return $this->_get(self::CUSTOMERS_ID);
    }

    /**
     * Set customers_id
     * @param string $customersId
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setCustomersId($customersId)
    {
        return $this->setData(self::CUSTOMERS_ID, $customersId);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Aureatelabs\CustomerApi\Api\Data\CustomersExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Aureatelabs\CustomerApi\Api\Data\CustomersExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get email
     * @return string|null
     */
    public function getEmail()
    {
        return $this->_get(self::EMAIL);
    }

    /**
     * Set email
     * @param string $email
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setEmail($email)
    {
        return $this->setData(self::EMAIL, $email);
    }

    /**
     * Get token
     * @return string|null
     */
    public function getToken()
    {
        return $this->_get(self::TOKEN);
    }

    /**
     * Set token
     * @param string $token
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setToken($token)
    {
        return $this->setData(self::TOKEN, $token);
    }

    /**
     * Get created
     * @return string|null
     */
    public function getCreated()
    {
        return $this->_get(self::CREATED);
    }

    /**
     * Set created
     * @param string $created
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setCreated($created)
    {
        return $this->setData(self::CREATED, $created);
    }

    /**
     * Get updated
     * @return string|null
     */
    public function getUpdated()
    {
        return $this->_get(self::UPDATED);
    }

    /**
     * Set updated
     * @param string $updated
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setUpdated($updated)
    {
        return $this->setData(self::UPDATED, $updated);
    }

    /**
     * Get brand
     * @return string|null
     */
    public function getBrand()
    {
        return $this->_get(self::BRAND);
    }

    /**
     * Set brand
     * @param string $brand
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setBrand($brand)
    {
        return $this->setData(self::BRAND, $brand);
    }

    /**
     * Get number
     * @return string|null
     */
    public function getNumber()
    {
        return $this->_get(self::NUMBER);
    }

    /**
     * Set number
     * @param string $number
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface
     */
    public function setNumber($number)
    {
        return $this->setData(self::NUMBER, $number);
    }
}

