<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\CustomerApi\Model;

use Aureatelabs\CustomerApi\Api\Data\CustomersInterface;
use Aureatelabs\CustomerApi\Api\Data\CustomersInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Customers extends \Magento\Framework\Model\AbstractModel
{

    protected $customersDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'aureatelabs_customerapi_customers';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param CustomersInterfaceFactory $customersDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Aureatelabs\CustomerApi\Model\ResourceModel\Customers $resource
     * @param \Aureatelabs\CustomerApi\Model\ResourceModel\Customers\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        CustomersInterfaceFactory $customersDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Aureatelabs\CustomerApi\Model\ResourceModel\Customers $resource,
        \Aureatelabs\CustomerApi\Model\ResourceModel\Customers\Collection $resourceCollection,
        array $data = []
    ) {
        $this->customersDataFactory = $customersDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve customers model with customers data
     * @return CustomersInterface
     */
    public function getDataModel()
    {
        $customersData = $this->getData();
        
        $customersDataObject = $this->customersDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $customersDataObject,
            $customersData,
            CustomersInterface::class
        );
        
        return $customersDataObject;
    }
}

