<?php
namespace Aureatelabs\PreOrderCustom\Observer;

class SalesOrderShipmentAfter implements \Magento\Framework\Event\ObserverInterface
{

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $shipment = $observer->getEvent()->getShipment();

        /** @var \Magento\Sales\Model\Order $order */
        $order = $shipment->getOrder();
        $status = $order->getStatus();

        if ($order->getInvoiceCollection()->count() && $status == "pre_ordered" && $order->getShipmentsCollection()->count()) {
            $items = $order->getAllItems();
            $isPreorder = '';
            foreach ($items as $item) {
                    $isPreorder = $item->getProduct()->getStockStatus();
                    if ($isPreorder === 'PreOrder'){
                        break;
                    }
            }
            if( $isPreorder === 'PreOrder'){
                $orderState = "complete";
                $order->setState($orderState)->setStatus($orderState);
                $order->save();
            }
        }
    }
}
