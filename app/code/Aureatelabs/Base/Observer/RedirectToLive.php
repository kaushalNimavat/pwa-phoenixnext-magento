<?php

namespace Aureatelabs\Base\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class RedirectToLive implements ObserverInterface
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlInterface;

    /**
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $response;

    /**
     * RedirectToLive constructor.
     * @param \Magento\Framework\App\ResponseInterface $response
     * @param \Magento\Framework\UrlInterface $urlInterface
     */
    public function __construct(
        \Magento\Framework\App\ResponseInterface $response,
        \Magento\Framework\UrlInterface $urlInterface
    ) {
        $this->response = $response;
        $this->urlInterface = $urlInterface;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $currentUrl = $this->urlInterface->getCurrentUrl();
		    if (strpos($currentUrl, 'p123payment') !== false || strpos($currentUrl, 'oauth') !== false || strpos($currentUrl, 'glew') !== false) {
            return;
        }

        $redirectUrl = str_replace(
            ['http://admin.phoenixnext.com', 'https://admin.phoenixnext.com'],
            ['https://www.phoenixnext.com', 'www.https://phoenixnext.com'],
            $currentUrl
        );
        $this->response->setRedirect($redirectUrl, 301);
    }
}
