<?php
namespace Aureatelabs\NewsletterAPI\Api;

/**
 * Newsletter interface.
 * @api
 */
interface NewsletterManagementInterface
{
    /**
     * Subscribe an email.
     *
     * @param string $email
     * @return \Aureatelabs\NewsletterAPI\Api\Data\NewsletterSubscribeInterface
     */
    public function subscribe($email);

     /**
     * Unsubscribe an email.
     *
     * @param string $email
     * @return \Aureatelabs\NewsletterAPI\Api\Data\NewsletterSubscribeInterface
     */
    public function unsubscribe($email);

    /**
     * Get detail of subscribe.
     *
     * @param string $email
     * @return \Aureatelabs\NewsletterAPI\Api\Data\NewsletterSubscribeInterface
     */
    public function get($email);
}
