<?php
namespace Aureatelabs\LOBOT\Cron;
use \Psr\Log\LoggerInterface;
use \TBA\LOBOT\Controller\LobotHelper\TbaComfirmStockHelper;

class ConfirmStockFromLobotOnSchedule
{
       protected $logger;
       protected $tbaComfirmStockHelper;
       protected $scopeConfig;

       const XML_PATH_CRON_STATUS = 'lobot_cron/stock_cron/enable';
       const XML_PATH_CRON_FROM_DATE = 'lobot_cron/stock_cron/disable_stock_cron_from_date';
       const XML_PATH_CRON_TO_DATE = 'lobot_cron/stock_cron/disable_stock_cron_to_date';

       public function __construct(
                LoggerInterface $logger,
                TbaComfirmStockHelper $tbaComfirmStockHelper,
	\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
       \Magento\Framework\App\Action\Context $context)
        {
           $this->logger = $logger;
	   $this->tbaComfirmStockHelper = $tbaComfirmStockHelper;
		 $this->scopeConfig = $scopeConfig;
        }

        function run()
        {
		$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

		$isEnabled = $this->scopeConfig->getValue(self::XML_PATH_CRON_STATUS, $storeScope);
		$fromDate = $this->scopeConfig->getValue(self::XML_PATH_CRON_FROM_DATE, $storeScope);
		$toDate = $this->scopeConfig->getValue(self::XML_PATH_CRON_TO_DATE, $storeScope);
		$date = strtotime(date('Y-m-d H:i:s'));
                $date1 = strtotime($fromDate);
                $date2 = strtotime($toDate. ' 23:59:59');

		if($isEnabled && ($date >= $date1 && $date <= $date2)){
                   return false;
		} else {
		   $this->tbaComfirmStockHelper->ConfirmStockFromLobot();
		}
        }
}


