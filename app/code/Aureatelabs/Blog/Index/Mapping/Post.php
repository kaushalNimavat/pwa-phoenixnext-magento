<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Blog
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Blog\Index\Mapping;

use Divante\VsbridgeIndexerCore\Api\MappingInterface;
use Divante\VsbridgeIndexerCore\Api\Mapping\FieldInterface;
use Magento\Framework\Event\ManagerInterface as EventManager;

/**
 * Class Post
 * @package Aureatelabs\Blog\Index\Mapping
 */
class Post implements MappingInterface
{
    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @var string
     */
    private $type;

    /**
     * @var array
     */
    private $textFields = [
        'title',
        'content',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'creation_time',
        'is_active'
    ];

    /**
     * CmsBlock constructor.
     *
     * @param EventManager $eventManager
     */
    public function __construct(EventManager $eventManager)
    {
        $this->eventManager = $eventManager;
    }

    /**
     * @inheritdoc
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @inheritdoc
     */
    public function getMappingProperties()
    {
        $properties = [
            'post_id' => ['type' => FieldInterface::TYPE_LONG],
            'is_active' => ['type' => FieldInterface::TYPE_TEXT],
            'featured_img' => ['type' => FieldInterface::TYPE_KEYWORD],
            'identifier' => ['type' => FieldInterface::TYPE_KEYWORD],
            'creation_time' => ['type' => FieldInterface::TYPE_TEXT],
        ];

        foreach ($this->textFields as $field) {
            $properties[$field] = ['type' => FieldInterface::TYPE_TEXT];
            if($field == "creation_time"){
                 $properties[$field]['fielddata'] = true;
            }
        }

        $mappingObject = new \Magento\Framework\DataObject();
        $mappingObject->setData('properties', $properties);

        $this->eventManager->dispatch(
            'elasticsearch_post_mapping_properties',
            ['mapping' => $mappingObject]
        );

        return $mappingObject->getData();
    }
}
