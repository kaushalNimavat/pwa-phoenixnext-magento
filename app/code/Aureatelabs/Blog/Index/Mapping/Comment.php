<?php

namespace Aureatelabs\Blog\Index\Mapping;

use Divante\VsbridgeIndexerCore\Api\Mapping\FieldInterface;
use Divante\VsbridgeIndexerCore\Api\MappingInterface;
use Magento\Framework\Event\ManagerInterface as EventManager;

class Comment implements MappingInterface
{
    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @var string
     */
    private $type;

    /**
     * @var array
     */
    private $textFields = [
        'author_nickname',
        'author_email',
        'text'
    ];

    /**
     * Category constructor.
     *
     * @param EventManager $eventManager
     */
    public function __construct(EventManager $eventManager)
    {
        $this->eventManager = $eventManager;
    }

    /**
     * @inheritdoc
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @inheritDoc
     */
    public function getMappingProperties()
    {
        $properties = [
            'comment_id' => ['type' => FieldInterface::TYPE_LONG],
            'parent_id' => ['type' => FieldInterface::TYPE_INTEGER],
            'post_id' => ['type' => FieldInterface::TYPE_INTEGER],
            'customer_id' => ['type' => FieldInterface::TYPE_INTEGER],
            'admin_id' => ['type' => FieldInterface::TYPE_INTEGER],
            'status' => ['type' => FieldInterface::TYPE_INTEGER],
            'author_type' => ['type' => FieldInterface::TYPE_INTEGER],
        ];

        foreach ($this->textFields as $field) {
            $properties[$field] = ['type' => FieldInterface::TYPE_TEXT];
        }

        $mappingObject = new \Magento\Framework\DataObject();
        $mappingObject->setData('properties', $properties);

        $this->eventManager->dispatch(
            'elasticsearch_post_comment_mapping_properties',
            ['mapping' => $mappingObject]
        );

        return $mappingObject->getData();
    }
}
