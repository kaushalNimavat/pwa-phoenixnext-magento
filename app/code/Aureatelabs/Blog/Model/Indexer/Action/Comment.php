<?php

namespace Aureatelabs\Blog\Model\Indexer\Action;

use Aureatelabs\Blog\Model\ResourceModel\Comment as CommentResource;
use Divante\VsbridgeIndexerCore\Indexer\RebuildActionInterface;

class Comment implements RebuildActionInterface
{
    /**
     * @var CommentResource
     */
    private $resourceModel;

    /**
     * Category constructor.
     * @param CommentResource $commentResource
     */
    public function __construct(
        CommentResource $commentResource
    ) {
        $this->resourceModel = $commentResource;
    }

    /**
     * @param int $storeId
     * @param array $ids
     * @return \Traversable
     */
    public function rebuild(int $storeId, array $ids): \Traversable
    {
        $lastCommentId = 0;
        do {
            $comments = $this->resourceModel->loadPostComments($storeId, $ids, $lastCommentId);
            foreach ($comments as $comment) {
                $lastCommentId = (int) $comment['comment_id'];
                $comment['comment_id'] = (int) $comment['comment_id'];
                $comment['parent_id'] = (int) $comment['parent_id'];
                $comment['post_id'] = (int) $comment['post_id'];
                $comment['customer_id'] = (int) $comment['customer_id'];
                $comment['admin_id'] = (int) $comment['admin_id'];
                $comment['status'] = (int) $comment['status'];
                $comment['author_type'] = (int) $comment['author_type'];

                yield $lastCommentId => $comment;
            }
        } while (!empty($comments));
    }
}
