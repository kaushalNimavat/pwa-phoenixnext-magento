<?php

namespace Aureatelabs\Blog\Model\Indexer\DataProvider;

use Aureatelabs\Blog\Model\ResourceModel\Comment;
use Divante\VsbridgeIndexerCore\Api\DataProviderInterface;

class BlogComments implements DataProviderInterface
{
    /**
     * @var Comment
     */
    private $comment;

    /**
     * @param Comment $comment
     */
    public function __construct(
        Comment $comment
    ) {
        $this->comment = $comment;
    }

    /**
     * @inheritDoc
     */
    public function addData(array $indexData, $storeId): array
    {
        $postComments = $this->getPostComments();
        foreach ($indexData as &$blogs) {
            if (isset($postComments[$blogs['post_id']]) && !empty($postComments[$blogs['post_id']])) {
                $commentIds = array_column($postComments[$blogs['post_id']], 'comment_id') ?? [];
                $commentIds = array_map('intval', $commentIds);
                $blogs['comment_ids'] = $commentIds;
            }
        }
        return $indexData;
    }

    /**
     * @return array
     */
    private function getPostComments(): array
    {
        $comments = $this->comment->loadPostComments();
        foreach ($comments as $comment) {
            $postComments[$comment['post_id']][] = $comment;
        }
        return $postComments ?? [];
    }
}
