<?php

namespace Aureatelabs\Blog\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;

class Comment
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * Comment constructor.
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resource = $resourceConnection;
    }

    /**
     * @param int $storeId
     * @param array $commentIds
     * @param int $fromId
     * @param int $limit
     * @return array
     */
    public function loadPostComments(int $storeId = 1, array $commentIds = [], int $fromId = 0, int $limit = 1000): array
    {
        $select = $this->getConnection()->select()->from([ 'comment' => $this->resource->getTableName('magefan_blog_comment')]);
        $select->where('comment.status = ?', 1);
        if (!empty($commentIds)) {
            $select->where('comment.comment_id IN (?)', $commentIds);
        }

        $select->where('comment.comment_id > ?', $fromId)
            ->limit($limit)
            ->order('comment.comment_id');

        return $this->getConnection()->fetchAll($select);
    }

    /**
     * @return AdapterInterface
     */
    private function getConnection(): AdapterInterface
    {
        return $this->resource->getConnection();
    }
}
