<?php

namespace Aureatelabs\Blog\Model;

class Comment extends \Magefan\Blog\Model\Comment
{
    const POST_ID = 'post_id';
    const PARENT_ID = 'parent_id';
    const CUSTOMER_ID = 'customer_id';
    const AUTHOR_NICKNAME = 'author_nickname';
    const AUTHOR_EMAIL = 'author_email';
    const TEXT = 'text';

    /**
     * @return int
     */
    public function getPostId(): int
    {
        return $this->_getData(self::POST_ID);
    }

    /**
     * @param int $value
     * @return Comment
     */
    public function setPostId(int $value): Comment
    {
        return $this->setData(self::POST_ID, $value);
    }

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->_getData(self::PARENT_ID);
    }

    /**
     * @param int $value
     * @return Comment
     */
    public function setParentId(int $value): Comment
    {
        return $this->setData(self::PARENT_ID, $value);
    }

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->_getData(self::CUSTOMER_ID);
    }

    /**
     * @param int $value
     * @return Comment
     */
    public function setCustomerId(int $value): Comment
    {
        return $this->setData(self::CUSTOMER_ID, $value);
    }

    /**
     * @return string
     */
    public function getAuthorNickname(): string
    {
        return $this->_getData(self::AUTHOR_NICKNAME);
    }

    /**
     * @param string $value
     * @return Comment
     */
    public function setAuthorNickname(string $value): Comment
    {
        return $this->setData(self::AUTHOR_NICKNAME, $value);
    }

    /**
     * @return string
     */
    public function getAuthorEmail(): string
    {
        return $this->_getData(self::AUTHOR_EMAIL);
    }

    /**
     * @param string $value
     * @return Comment
     */
    public function setAuthorEmail(string $value): Comment
    {
        return $this->setData(self::AUTHOR_EMAIL, $value);
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->_getData(self::TEXT);
    }

    /**
     * @param string $value
     * @return Comment
     */
    public function setText(string $value): Comment
    {
        return $this->setData(self::TEXT, $value);
    }
}
