<?php

namespace Aureatelabs\Blog\Plugin\Indexer\Post\Save;

use Aureatelabs\Blog\Model\Indexer\CommentProcessor;
use Aureatelabs\Blog\Model\Indexer\PostProcessor;
use Magefan\Blog\Model\Comment;

class UpdateComment
{
    /**
     * @var CommentProcessor
     */
    private $commentProcessor;

    /**
     * @var PostProcessor
     */
    private $postProcessor;

    /**
     * Save constructor.
     * @param PostProcessor $postProcessor
     * @param CommentProcessor $commentProcessor
     */
    public function __construct(
        PostProcessor $postProcessor,
        CommentProcessor $commentProcessor
    ) {
        $this->postProcessor = $postProcessor;
        $this->commentProcessor = $commentProcessor;
    }

    /**
     * @param Comment $comment
     * @param Comment $result
     * @return Comment
     */
    public function afterAfterSave(Comment $comment, Comment $result): Comment
    {
        $result->getResource()->addCommitCallback(function () use ($result) {
            $this->commentProcessor->reindexRow($result->getId());
            $this->postProcessor->reindexRow($result->getPostId());
        });
        return $result;
    }

    /**
     * @param Comment $comment
     * @param Comment $result
     * @return Comment
     */
    public function afterAfterDeleteCommit(Comment $comment, Comment $result): Comment
    {
        $this->commentProcessor->reindexRow($result->getId());
        return $result;
    }
}
