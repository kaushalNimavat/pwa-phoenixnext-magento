<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Blog
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Blog\Plugin\Indexer\Post\Save;

use Aureatelabs\Blog\Model\Indexer\PostProcessor;
use Magefan\Blog\Model\Post;

/**
 * Class UpdatePost
 * @package Aureatelabs\Blog\Plugin\Indexer\Post\Save
 */
class UpdatePost
{
    /**
     * @var PostProcessor
     */
    private $postProcessor;

    /**
     * Save constructor.
     *
     * @param PostProcessor $postProcessor
     */
    public function __construct(PostProcessor $postProcessor)
    {
        $this->postProcessor = $postProcessor;
    }

    /**
     * @param Post $post
     * @param Post $result
     *
     * @return Post
     */
    public function afterAfterSave(Post $post, Post $result)
    {
        $result->getResource()->addCommitCallback(function () use ($post) {
            $this->postProcessor->reindexRow($post->getId());
        });

        return $result;
    }

    /**
     * @param Post $post
     * @param Post $result
     *
     * @return Post
     */
    public function afterAfterDeleteCommit(Post $post, Post $result)
    {
        $this->postProcessor->reindexRow($post->getId());

        return $result;
    }
}
