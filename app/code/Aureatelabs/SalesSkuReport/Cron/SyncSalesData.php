<?php

/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_SalesSkuReport
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */

namespace Aureatelabs\SalesSkuReport\Cron;

class SyncSalesData
{

    protected $scopeConfig;

    protected $orderCollectionFactory;

    protected $orderRepository;

    const MODULE_ENABLE = 'skureport/general/enable';

    const MONTH = 'skureport/general/month';

    const YEAR = 'skureport/general/year';

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Aureatelabs\SalesSkuReport\Model\SkusalesFactory $skusalesFactory,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->skusalesFactory = $skusalesFactory;
        $this->resource = $resource;
    }

    public function execute()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $moduleEnable = $this->scopeConfig->getValue(self::MODULE_ENABLE, $storeScope);
        $allorderItemsData = [];
        $connection = $this->resource->getConnection();
        $orderItems =  $this->resource->getTableName('sales_order_item');
        $query = $connection->select()->from(['order' => $orderItems], ['order_id', 'sku']);
        $orderItemsData = $connection->fetchAll($query);
        foreach ($orderItemsData as $orderItem) {
            $allorderItemsData[$orderItem['order_id']][] = $orderItem;
        }
        if ($moduleEnable) {
            $currentDate = date("Y-m-d");
            $orderCollection = $this->getOrderCollectionByDateRange($currentDate);
            $i = 0;
            foreach ($orderCollection as $order) {
                $customerId = $order['customer_id'];
                $customerGender = $order['customer_gender'];
                $customerDob = $order['customer_dob'];
                $orderId = $order['entity_id'];
                $orderDate = $order['created_at'];
                $customerAgeYear = (date('Y') - date('Y', strtotime($customerDob)));
                if ($customerAgeYear < 13) {
                    $customerAge = 'Under 13';
                } else if ($customerAgeYear <= 17 && $customerAgeYear >= 13) {
                    $customerAge = '13-17';
                } else if ($customerAgeYear <= 24 && $customerAgeYear >= 18) {
                    $customerAge = '18-24';
                } else if ($customerAgeYear <= 34 && $customerAgeYear >= 25) {
                    $customerAge = '25-34';
                } else if ($customerAgeYear <= 44 && $customerAgeYear >= 35) {
                    $customerAge = '35-44';
                } else if ($customerAgeYear <= 54 && $customerAgeYear >= 45) {
                    $customerAge = '45-54';
                } else if ($customerAgeYear <= 64 && $customerAgeYear >= 55) {
                    $customerAge = '55-64';
                } else if ($customerAgeYear >= 65) {
                    $customerAge = '65 Over';
                }
                if (isset($allorderItemsData[$orderId])) {
                    foreach ($allorderItemsData[$orderId] as $item) {
                        $skuSalesFactorty = $this->skusalesFactory->create();

                        $collection = $skuSalesFactorty->getCollection()->addFieldToFilter('order_id',  ['eq' => $orderId])->addFieldToFilter('sku',  ['eq' => $item["sku"]]);

                        if ($collection->getSize()) {
                            foreach ($collection as $record) {

                                $savedData = $record->getCustomerDob();

                                if ($record->getCustomerDob() != $customerAge || $record->getCustomerGender() != $customerGender) {
                                    $entityId = $record->getEntityId();
                                    $loadRecord = $skuSalesFactorty->load($entityId);
                                    $loadRecord->setCustomerDob($customerAge);
                                    $loadRecord->setCustomerGender($customerGender);
                                    $loadRecord->save();
                                    $i++;
                                }
                            }
                        } else {
                            $salesFactory = $this->skusalesFactory->create();
                            $sku = $item["sku"];
                            $salesFactory->setOrderId($orderId);
                            $salesFactory->setOrderDate($orderDate);
                            $salesFactory->setCustomerId($customerId);
                            $salesFactory->setCustomerDob($customerAge);
                            $salesFactory->setCustomerGender($customerGender);
                            $salesFactory->setSku($sku);
                            $salesFactory->save();
                            $i++;
                        }
                    }
                }
            }
        }
    }

    public function getOrderCollectionByDateRange($previousdate)
    {
        $currentDate = date("Y-m-d");
        $currentTime = date("H:i:s");

        $startDate = date("Y-m-d 00:00:00", strtotime($previousdate));
        $endDate = date("Y-m-d 23:59:59", strtotime($currentDate . $currentTime));

        $connection = $this->resource->getConnection();
        $orderMain = $this->resource->getTableName('sales_order');
        $query = $connection->select()->from(['order' => $orderMain], ['entity_id', 'customer_id', 'customer_dob', 'customer_gender', 'created_at'])->where("order.created_at BETWEEN '" . $startDate . "' AND '" . $endDate . "'");
        $orderItemsData = $connection->fetchAll($query);
        return $orderItemsData;
    }
}
