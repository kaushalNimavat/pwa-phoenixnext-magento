<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_SalesSkuReport
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */

 namespace Aureatelabs\SalesSkuReport\Controller\Adminhtml\Report;

 use Magento\Framework\App\Filesystem\DirectoryList;

 class ExportSalesCsv extends \Magento\Backend\App\Action {

    protected $_fileFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory
    ) {
        $this->_fileFactory = $fileFactory;
        parent::__construct($context);
    }
     
    public function execute()
    {
        $fileName = 'salessku.csv';
        $grid = $this->_view->getLayout()->createBlock(\Aureatelabs\SalesSkuReport\Block\Adminhtml\Grid\Grid::class);
        return $this->_fileFactory->create($fileName, $grid->getCsvFile(), DirectoryList::VAR_DIR);
    }
 }