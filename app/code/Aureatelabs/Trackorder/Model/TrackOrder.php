<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Trackorder
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Trackorder\Model;

use Aureatelabs\Trackorder\Api\TrackOrderInterface;

/**
 * Class TrackOrder
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class TrackOrder implements TrackOrderInterface
{
    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $cartId;

    /**
     * Get status.
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get cart id.
     *
     * @return string|null
     */
    public function getCartId()
    {
        return $this->cartId;
    }

    /**
     * Set cart id.
     *
     * @param string $cartId
     */
    public function setCartId($cartId)
    {
        $this->cartId = $cartId;
    }
}
