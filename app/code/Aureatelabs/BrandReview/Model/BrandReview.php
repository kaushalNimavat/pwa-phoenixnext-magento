<?php

namespace Aureatelabs\BrandReview\Model;

use Aureatelabs\BrandReview\Api\Data\BrandReviewExtensionInterface;
use Aureatelabs\BrandReview\Api\Data\BrandReviewInterface;
use Aureatelabs\BrandReview\Api\Data\BrandReviewInterfaceFactory;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\LocalizedException;

class BrandReview extends \Aureatelabs\BrandReview\Model\AbstractModel implements BrandReviewInterface
{
    /**
     * @var BrandReviewInterfaceFactory
     */
    private $brandReviewInterfaceFactory;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param BrandReviewInterfaceFactory $brandReviewInterfaceFactory
     * @param ResourceModel\BrandReview $resource
     * @param ResourceModel\BrandReview\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        DataObjectHelper $dataObjectHelper,
        BrandReviewInterfaceFactory $brandReviewInterfaceFactory,
        \Aureatelabs\BrandReview\Model\ResourceModel\BrandReview $resource,
        \Aureatelabs\BrandReview\Model\ResourceModel\BrandReview\Collection $resourceCollection,
        array $data = []
    ) {
        $this->brandReviewInterfaceFactory = $brandReviewInterfaceFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * @inheritDoc
     */
    public function getBrandId()
    {
        return $this->_getData(self::BRAND_ID);
    }

    /**
     * @inheritDoc
     */
    public function setBrandId($brandId)
    {
        return $this->setData(self::BRAND_ID, $brandId);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerId()
    {
        return $this->_getData(self::CUSTOMER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerName()
    {
        return $this->_getData(self::CUSTOMER_NAME);
    }

    /**
     * @inheritDoc
     */
    public function setCustomerName($customerName)
    {
        return $this->setData(self::CUSTOMER_NAME, $customerName);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerEmail()
    {
        return $this->_getData(self::CUSTOMER_EMAIL);
    }

    /**
     * @inheritDoc
     */
    public function setCustomerEmail($customerEmail)
    {
        return $this->setData(self::CUSTOMER_EMAIL, $customerEmail);
    }

    /**
     * @inheritDoc
     */
    public function getTitle()
    {
        return $this->_getData(self::TITLE);
    }

    /**
     * @inheritDoc
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * @inheritDoc
     */
    public function getDescription()
    {
        return $this->_getData(self::DESCRIPTION);
    }

    /**
     * @inheritDoc
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @inheritDoc
     */
    public function getRatings()
    {
        return $this->_getData(self::RATINGS);
    }

    /**
     * @inheritDoc
     */
    public function setRatings($ratings)
    {
        return $this->setData(self::RATINGS, $ratings);
    }

    /**
     * @inheritDoc
     */
    public function getStatus()
    {
        return $this->_getData(self::STATUS);
    }

    /**
     * @inheritDoc
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * @inheritDoc
     */
    public function setExtensionAttributes(BrandReviewExtensionInterface $extensionAttributes)
    {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * @return BrandReviewInterface
     */
    public function getDataModel(): BrandReviewInterface
    {
        $brandReviewData = $this->getData();

        $brandReviewDataObject = $this->brandReviewInterfaceFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $brandReviewDataObject,
            $brandReviewData,
            BrandReviewInterface::class
        );

        return $brandReviewDataObject;
    }

    /**
     * Check product options and type options and save them, too
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @throws LocalizedException
     */
    public function beforeSave()
    {
        if ($this->getData(self::BRAND_ID) === null) {
            throw new LocalizedException(__('Brand Id is required'));
        }
        if ($this->getData(self::CUSTOMER_ID) === null) {
            throw new LocalizedException(__('Customer Id is required'));
        }
        if ($this->getData(self::CUSTOMER_NAME) === null) {
            throw new LocalizedException(__('Customer Name is required'));
        }
        if ($this->getData(self::CUSTOMER_EMAIL) === null) {
            throw new LocalizedException(__('Customer Email is required'));
        }
        if ($this->getData(self::RATINGS) === null) {
            throw new LocalizedException(__('Ratings is required'));
        }
        if ($this->getData(self::STATUS) === null) {
            $this->getData(self::STATUS, 0);
        }
        parent::beforeSave();
    }
}
