<?php

namespace Aureatelabs\BrandReview\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class BrandReview extends AbstractDb
{
    /**
     * BrandReview constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('aureatelabs_brand_review', 'entity_id');
    }
}
