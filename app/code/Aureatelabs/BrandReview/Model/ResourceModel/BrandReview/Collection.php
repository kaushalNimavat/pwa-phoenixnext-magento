<?php

namespace Aureatelabs\BrandReview\Model\ResourceModel\BrandReview;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Aureatelabs\BrandReview\Model\BrandReview::class,
            \Aureatelabs\BrandReview\Model\ResourceModel\BrandReview::class
        );
    }
}
