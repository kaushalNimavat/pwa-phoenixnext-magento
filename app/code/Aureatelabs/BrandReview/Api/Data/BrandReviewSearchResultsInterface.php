<?php

namespace Aureatelabs\BrandReview\Api\Data;

interface BrandReviewSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get Review list.
     * @return \Aureatelabs\BrandReview\Api\Data\BrandReviewInterface[]
     */
    public function getItems();

    /**
     * Set Review list.
     * @param \Aureatelabs\BrandReview\Api\Data\BrandReviewInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
