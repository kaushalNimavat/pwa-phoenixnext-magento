<?php

namespace Aureatelabs\BrandReview\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface BrandReviewInterface extends ExtensibleDataInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const BRAND_ID = 'brand_id';
    const CUSTOMER_ID = 'customer_id';
    const CUSTOMER_NAME = 'customer_name';
    const CUSTOMER_EMAIL = 'customer_email';
    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const RATINGS = 'ratings';
    const STATUS = 'status';
    const CREATED_AT = 'created_at';

    /**
     * @return mixed
     */
    public function getBrandId();

    /**
     * @param $brandId
     * @return mixed
     */
    public function setBrandId($brandId);

    /**
     * @return mixed
     */
    public function getCustomerId();

    /**
     * @param $customerId
     * @return mixed
     */
    public function setCustomerId($customerId);

    /**
     * @return mixed
     */
    public function getCustomerName();

    /**
     * @param $customerName
     * @return mixed
     */
    public function setCustomerName($customerName);

    /**
     * @return mixed
     */
    public function getCustomerEmail();

    /**
     * @param $customerEmail
     * @return mixed
     */
    public function setCustomerEmail($customerEmail);

    /**
     * @return mixed
     */
    public function getTitle();

    /**
     * @param $title
     * @return mixed
     */
    public function setTitle($title);

    /**
     * @return mixed
     */
    public function getDescription();

    /**
     * @param $description
     * @return mixed
     */
    public function setDescription($description);

    /**
     * @return int
     */
    public function getRatings();

    /**
     * @param $ratings
     * @return int
     */
    public function setRatings($ratings);

    /**
     * @return mixed
     */
    public function getStatus();

    /**
     * @param $status
     * @return mixed
     */
    public function setStatus($status);

    /**
     * @return mixed
     */
    public function setCreatedAt($createdAt);

    /**
     * @return mixed
     */
    public function getCreatedAt();

    /**
     * @return \Aureatelabs\BrandReview\Api\Data\BrandReviewExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * @param \Aureatelabs\BrandReview\Api\Data\BrandReviewExtensionInterface $extensionAttributes
     * @return void
     */
    public function setExtensionAttributes(BrandReviewExtensionInterface $extensionAttributes);
}
