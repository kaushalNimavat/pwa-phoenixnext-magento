<?php

namespace Aureatelabs\BrandReview\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface BrandReviewRepositoryInterface
{
    /**
     * @param Data\BrandReviewInterface $brandReview
     * @return \Aureatelabs\BrandReview\Api\Data\BrandReviewInterface
     */
    public function save(
        Data\BrandReviewInterface $brandReview
    );

    /**
     * @param $id
     * @return mixed
     */
    public function get($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Aureatelabs\BrandReview\Api\Data\BrandReviewSearchResultsInterface
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * @param Data\BrandReviewInterface $brandReview
     * @return mixed
     */
    public function delete(
        Data\BrandReviewInterface $brandReview
    );

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);
}
