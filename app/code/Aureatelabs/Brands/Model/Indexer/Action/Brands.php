<?php

/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Brands
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */

namespace Aureatelabs\Brands\Model\Indexer\Action;

use Aureatelabs\Brands\Model\ImageUploader;
use Aureatelabs\Brands\Model\ResourceModel\Brand as BrandResource;
use Divante\VsbridgeIndexerCore\Indexer\RebuildActionInterface;
use Magento\Cms\Model\Template\FilterProvider;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Brands
 * @package Aureatelabs\Brands\Model\Indexer\Action
 */
class Brands implements RebuildActionInterface
{
    /**
     * @var BrandResource
     */
    private $resourceModel;

    /**
     * @var ImageUploader
     */
    protected $imageUploader;

    /**
     * @var FilterProvider
     */
    private $filterProvider;

    /**
     * @param BrandResource $brandResource
     * @param ImageUploader $imageUploader
     * @param FilterProvider $filterProvider
     */
    public function __construct(
        BrandResource $brandResource,
        ImageUploader $imageUploader,
        FilterProvider $filterProvider
    ) {
        $this->resourceModel = $brandResource;
        $this->imageUploader = $imageUploader;
        $this->filterProvider = $filterProvider;
    }

/*    public function rebuild($storeId = 1, array $brandsIds = [])
    {

        $this->areaList->getArea(Area::AREA_FRONTEND)->load(Area::PART_DESIGN);
        $brandss = $this->resourceModel->loadBrands($storeId, $brandsIds);
        $collection = $this->collectionFactory->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
            ->load();
        $enableProducts = [];
        foreach ($collection as $product) {
            array_push($enableProducts, $product->getId());
        }
        foreach ($brandss as $brands) {
            $lastBrandsId = $brands['brand_id'];
            $brands['id'] = $brands['brand_id'];
            if ($brands["brand_products"] != NULL) {
                $brandProductIds = explode(",", $brands["brand_products"]);
                $availableProducts = [];
                foreach ($brandProductIds as $brandProduct) {
                    if (in_array($brandProduct, $enableProducts)) {
                        array_push($availableProducts, $brandProduct);
                    }
                }
                if (!empty($availableProducts)) {
                    $finalProduct = implode(",", $availableProducts);
                    $brands["brand_products"] = $finalProduct;
                } else {
                    $brands["brand_products"] = '';
                }
            }
            $brands['brand_description'] = $this->filterProvider->getPageFilter()->filter($brands['brand_description']);
            $brands['image'] = $this->imageUploader->getFileUrl($brands['brand_image']);
            $brands['brand_banner_image_url'] = $this->imageUploader->getFileUrl($brands['brand_detail_image']);
            $brands['brand_banner_mobile_image_url'] = $this->imageUploader->getFileUrl($brands['brand_mobile_image']);
            $brands['sort_order'] = (int)$brands['sort_order'];
            yield $lastBrandsId => $brands;
        }
    }*/
    /**
     * @param int $storeId
     * @param array $ids
     * @return \Traversable
     * @throws NoSuchEntityException
     * @throws \Exception
     */
    public function rebuild(int $storeId, array $ids): \Traversable
    {
        $lastBrandId = 0;
        do {
            $brands = $this->resourceModel->loadBrands($storeId, $ids, $lastBrandId);
            foreach ($brands as $brand) {
                $lastBrandsId = $brand['brand_id'];
                $brand['id'] = $brand['brand_id'];

                $brand['image'] = $brand['brand_image'] ? $this->imageUploader->getFileUrl($brand['brand_image']) : null;
                $brand['brand_banner_image_url'] = $brand['brand_detail_image'] ? $this->imageUploader->getFileUrl($brand['brand_detail_image']) : null;
		        $brand['brand_banner_mobile_image_url'] = $brand['brand_mobile_image'] ? $this->imageUploader->getFileUrl($brand['brand_mobile_image']) : null;
		        $brand['relatedpost'] = $this->resourceModel->loadRelatedBlogs($brand['id']);

                $brand['active'] = (bool)$brand['is_active'];
                unset($brand['is_active']);
                yield $lastBrandsId => $brand;
            }
        } while (!empty($banners));
    }
}
