<?php

namespace Aureatelabs\LabelImage\Plugin;

use Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Catalog\Helper\Data;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\Session;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Mirasvit\CatalogLabel\Block\Product\Label;
use Mirasvit\CatalogLabel\Model\Placeholder;
use Magento\Catalog\Model\ProductFactory;
use Mirasvit\CatalogLabel\Model\ResourceModel\Label\CollectionFactory;
use Mirasvit\RewardsCatalog\Helper\Earn;

class AttributeData
{
    /**
     * @var array
     */
    protected $parsedAttribute = [
        'description',
        'short_description'
    ];

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Label
     */
    private $label;

    /**
     * @var Placeholder
     */
    private $placeholder;

    /**
     * @var CollectionFactory
     */
    private $labelCollectionFactory;

    /**
     * @var ProductFactory
     */
    private $productLoader;

    /**
     * @var Earn
     */
    private $earnHelper;

    /**
     * @var Configurable
     */
    private $configurable;

    /**
     * @var Data
     */
    private $catalogHelper;

    /**
     * @param Data $catalogHelper
     * @param StoreManagerInterface $storeManager
     * @param Label $label
     * @param Configurable $configurable
     * @param Placeholder $placeholder
     * @param CollectionFactory $labelCollectionFactory
     * @param ProductFactory $productLoader
     * @param Earn $earnHelper
     * @param Session $customerSession
     */
    public function __construct(
        Data $catalogHelper,
        StoreManagerInterface $storeManager,
        Label $label,
        Configurable $configurable,
        Placeholder $placeholder,
        CollectionFactory $labelCollectionFactory,
        ProductFactory $productLoader,
        Earn $earnHelper,
        Session $customerSession
    ) {
        $this->catalogHelper = $catalogHelper;
        $this->label = $label;
        $this->storeManager = $storeManager;
        $this->configurable = $configurable;
        $this->placeholder = $placeholder;
        $this->labelCollectionFactory = $labelCollectionFactory;
        $this->productLoader = $productLoader;
        $this->earnHelper = $earnHelper;
        $this->customerSession = $customerSession;
    }

    /**
     * @param \Divante\VsbridgeIndexerCatalog\Model\Indexer\DataProvider\Product\AttributeData $subject
     * @param $result
     * @param array $indexData
     * @param $storeId
     * @return mixed
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function afterAddData(
        \Divante\VsbridgeIndexerCatalog\Model\Indexer\DataProvider\Product\AttributeData $subject,
        $result,
        array $indexData,
        $storeId
    ) {
        $configurable = array_column($result, 'sku', 'id');
        $customer = $this->customerSession->getCustomer();
        $websiteId = $this->storeManager->getDefaultStoreView()->getWebsiteId();
        $media_url = $this->storeManager->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'cataloglabel';

        $labels = [];
        if ($this->label->getLabel()) {
            $labels[] = $this->label->getLabel();
        } else {
            $placeHolderIds = [];
            $viewPlaceholders = $this->placeholder->getCollection()->addFieldTofilter('is_active', 1);
            foreach ($viewPlaceholders as $placeholder) {
                $placeHolderIds[] = $placeholder->getId();
            }
            $labels = $this->labelCollectionFactory->create()
                ->addActiveFilter()
                ->addCustomerGroupFilter($this->customerSession->getCustomerGroupId())
                ->addStoreFilter($this->storeManager->getStore())
                ->addFieldToFilter('placeholder_id', ['in' => $placeHolderIds]);
            //it will be applied only if more than one labels are in the same position
            $labels->getSelect()->order('sort_order ASC');
        }

        $templateProcessor = $this->catalogHelper->getPageTemplateProcessor();
        foreach ($result as &$product) {
            $_product = $this->productLoader->create()->load($product['id']);
            $results = $html = [];

            /** @var \Mirasvit\CatalogLabel\Model\Label $label */
            foreach ($labels as $label) {
                $results = array_merge($results, $label->getDisplays($_product));
            }
            foreach ($results as $display) {
                $display->setType($this->label->getType());
                $html[] = $display->getData();
            }
            if (!empty($html)) {
                foreach($html as $idy => $valueId){
                    $product['product_sticker_img'][$idy] = '';
                    if ($valueId['list_image']) {
                        $product['label_image'][$idy] = $media_url . $valueId['list_image'];
                    }
                }
            }

            $points = $this->earnHelper->getProductPoints($_product, $customer, $websiteId);
            $product['product_reward_point'] = $points;
            if ($product['type_id'] != \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                $parentId = $this->getParentProductId($product['id']);
                $product['parent_id'] = $parentId ?? null;
                $product['parent_sku'] = $configurable[$parentId] ?? null;
            }

            foreach ($this->parsedAttribute as $attribute) {
                if (isset($product[$attribute])) {
                    $product[$attribute] = $templateProcessor->filter($product[$attribute]);
                }
            }
        }
        return $result;
    }

    /**
     * @param int $childId
     * @return int
     */
    public function getParentProductId(int $childId): ?int
    {
        $parentConfigObject = $this->configurable->getParentIdsByChild($childId);
        if ($parentConfigObject) {
            return (int) $parentConfigObject[0];
        }
        return null;
    }
}
