<?php
/*
 * Created by 2C2P
 * Date 20 June 2017
 * This Response action method is responsible for handle the 2c2p payment gateway response.
 */

namespace TBA\P123Payment\Controller\Payment;

use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Session;
use Magento\Sales\Model\OrderFactory;
use Magento\Customer\Model\Session as Customer;
use P2c2p\P2c2pPayment\Helper\Checkout;
use P2c2p\P2c2pPayment\Helper\P2c2pRequest;
use P2c2p\P2c2pPayment\Helper\P2c2pMeta;
use P2c2p\P2c2pPayment\Helper\P2c2pHash;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Catalog\Model\Session as catalogSession;
use Magento\Sales\Api\OrderManagementInterface;

class Ipn extends \P2c2p\P2c2pPayment\Controller\AbstractCheckoutApiAction
{

  	protected $objectManager;

	protected $orderManagement;

	private $url  = 'https://www.phoenixnext.com';
	private $path = '/var/www/www.phoenixnext.com/';

	public function __construct(
		Context $context,
        Session $checkoutSession,
		OrderFactory $orderFactory,
        Customer $customer,
		Checkout $checkoutHelper,
        P2c2pRequest $p2c2pRequest,
		P2c2pMeta $p2c2pMeta,
        P2c2pHash $p2c2pHash,
		ScopeConfigInterface $configSettings,
        catalogSession $catalogSession,
		OrderManagementInterface $orderManagement
	) {

		$this->orderManagement = $orderManagement;
        parent::__construct(
			$context,
			$checkoutSession,
			$orderFactory,
			$customer,
			$checkoutHelper,
			$p2c2pRequest,
			$p2c2pMeta,
			$p2c2pHash,
			$configSettings,
			$catalogSession
		);
    }

	/**
	 * Write log into a file
	 */
	private function log($message)
	{
		$date = date('Y-m-d h:i:s');
	    $orderId = $_REQUEST["order_id"];
	    $filename = $this->path . '2c2p/backend_'.$orderId.'.txt';

		file_put_contents($filename, $date . ' : ' . $message.PHP_EOL, FILE_APPEND | LOCK_EX);
	}

	/**
	 * Redirect to website
	 */
	private function redirectTo($path, $query)
	{
		$url = implode('?', array($this->url . $path, $query));
		header("Location: $url");
		die();
	}

	/**
	 * Process response from 2C2P and update order status
	 */
	public function execute()
	{
		try {
			$_REQUEST = array_merge($_REQUEST, $_POST, $_GET);
			if(!array_key_exists('merchant_id', $_REQUEST)) {
				$this->log('Can\'t receive data from 2c2p&123 Service.');
				$this->redirectTo('/', 'r=payment-failed');
			}

			$hashHelper   	= $this->getHashHelper();
			$configHelper 	= $this->getConfigSettings();
			$SECRETKEY 		= $configHelper['secretKey'];
			$signatureXml	= $_REQUEST["hash_value"];
			$orderId		= $_REQUEST["order_id"];
			$paymentStatusCode = $_REQUEST["channel_response_code"];

			$log = PHP_EOL . PHP_EOL;
			$log .= '--------------------------------------------------' . PHP_EOL;
			$log .= '     2c2p Receiving Starting.' . PHP_EOL;
			$log .= '--------------------------------------------------' . PHP_EOL;
			$this->log($log);
			$this->log("2c2p Receiving order_id : " . $orderId);

			foreach ($_REQUEST as $index => $node) {
				$this->log("2c2p Receiving :: $index => $node");
			}

			// Get the object of current order.
			$order = $this->getOrderDetailByOrderId($orderId);
			$signatureHash = $this->generateHashCode($SECRETKEY);
			$isValidHash  = (strcmp(strtolower($signatureXml), strtolower($signatureHash))==0) ? true : false;

			// Check whether hash value is valid or not If not valid then redirect to home page when hash value is wrong.
			if(!$isValidHash) {
				if ($order->getId()) {
					$order->setState(\Magento\Sales\Model\Order::STATUS_FRAUD);
					$order->setStatus(\Magento\Sales\Model\Order::STATUS_FRAUD);
					$order->save();
				}
				$this->log("inValidHash.");
				$this->redirectTo('/', 'r=payment-failed');
			}

			$metaDataHelper = $this->getMetaDataHelper();
			$metaDataHelper->savePaymentGetawayResponse($_REQUEST, $order->getCustomerId());

			//check payment status according to payment response.
			if(strcasecmp($paymentStatusCode, "000") == 0 || strcasecmp($paymentStatusCode, "00") == 0) {

				//IF payment status code is success
				if(!empty($order->getCustomerId()) && !empty($_REQUEST['stored_card_unique_id'])) {
					$intCustomerId = $order->getCustomerId();
					$boolIsFound = false;

					// Fetch data from database by using the customer ID.
					$objTokenData = $metaDataHelper->getUserToken($intCustomerId);
					$arrayTokenData = array(
						'user_id' => $intCustomerId,
						'stored_card_unique_id' => $_REQUEST['stored_card_unique_id'],
						'masked_pan' => $_REQUEST['masked_pan'],
						'created_time' =>  date("Y-m-d H:i:s")
					);

					/* 
					Iterate foreach and check whether token key is present into p2c2p_token table or not.
					If token key is already present into database then prevent insert entry otherwise insert token entry into database.
					*/				   
					foreach ($objTokenData as $key => $value) {
						if(strcasecmp($value->getData('masked_pan'), $_REQUEST['masked_pan']) == 0 && 
						strcasecmp($value->getData('stored_card_unique_id'), $_REQUEST['stored_card_unique_id']) == 0) {
							$boolIsFound = true;
							break;
						}
					}

					if(!$boolIsFound) {
						$metaDataHelper->saveUserToken($arrayTokenData);					
					}
				}

				//Set the complete status when payment is completed.
				if ($order->getId()) {
					$order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING);
					$order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
					$order->save();
				}

				$this->log("Set the complete status when payment is completed.");
				$data = array(
					'transaction_ref' => $_REQUEST["transaction_ref"],
					'payment_status' => $_REQUEST["payment_status"],
					'order_id' => $_REQUEST["order_id"],
					'amount' => $_REQUEST["amount"]
				);
				$this->redirectTo('/thank-you', http_build_query($data));

			} else if(strcasecmp($paymentStatusCode, "001") == 0) {

				//Set the Pending payment status when payment is pending. like 123 payment type.
				if ($order->getId()) {
					$order->setState("Pending_2C2P");
					$order->setStatus("Pending_2C2P");
					$order->save();
				}

				$this->log("Set the Pending payment status when payment is pending.");

			} else {

				//If payment status code is cancel/Error/other.
				if ($order->getId()) {

					$this->orderManagement->cancel($order->getId());

					$order->setState(\Magento\Sales\Model\Order::STATE_CANCELED);
					$order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
					$order->save();
				}

				$this->log("payment status code is cancel/Error/other.");
				$this->executeCancelAction();
			}

			$this->redirectTo('/', 'r=payment-failed');

		} catch (\Exception $e) {
			$this->redirectTo('/', 'r=payment-failed');
		}
	}

	/**
	 * Generate Hash code to verify response
	 */
	private function generateHashCode($SECRETKEY)
	{
		// Get Payment getway response to variable.
		$version				= $_REQUEST["version"];
		$request_timestamp		= $_REQUEST["request_timestamp"];
		$merchant_id			= $_REQUEST["merchant_id"];
		$currency				= $_REQUEST["currency"];
		$order_id				= $_REQUEST["order_id"];
		$amount					= $_REQUEST["amount"];
		$invoice_no				= $_REQUEST["invoice_no"];
		$transaction_ref		= $_REQUEST["transaction_ref"];
		$approval_code			= $_REQUEST["approval_code"];
		$eci					= $_REQUEST["eci"];
		$transaction_datetime	= $_REQUEST["transaction_datetime"];
		$payment_channel		= $_REQUEST["payment_channel"];
		$payment_status			= $_REQUEST["payment_status"];
		$channel_response_code	= $_REQUEST["channel_response_code"];
		$channel_response_desc	= $_REQUEST["channel_response_desc"];
		$masked_pan				= $_REQUEST["masked_pan"];
		$stored_card_unique_id	= $_REQUEST["stored_card_unique_id"];
		$backend_invoice		= $_REQUEST["backend_invoice"];
		$paid_channel			= isset($_REQUEST["paid_channel"]) ? $_REQUEST["paid_channel"] : '';
		$recurring_unique_id	= $_REQUEST["recurring_unique_id"];
		$paid_agent				= isset($_REQUEST["paid_agent"]) ? $_REQUEST["paid_agent"] : '';
		$payment_scheme			= $_REQUEST["payment_scheme"];
		$user_defined_1			= $_REQUEST["user_defined_1"];
		$user_defined_2			= $_REQUEST["user_defined_2"];
		$user_defined_3			= $_REQUEST["user_defined_3"];
		$user_defined_4			= $_REQUEST["user_defined_4"];
		$user_defined_5			= $_REQUEST["user_defined_5"];
		$browser_info			= $_REQUEST["browser_info"];
		$ippPeriod				= $_REQUEST["ippPeriod"];
		$ippInterestType		= $_REQUEST["ippInterestType"];
		$ippInterestRate		= $_REQUEST["ippInterestRate"];
		$ippMerchantAbsorbRate	= $_REQUEST["ippMerchantAbsorbRate"];
		$process_by				= $_REQUEST["process_by"];
		$sub_merchant_list		= $_REQUEST["sub_merchant_list"];
		$hash_value				= $_REQUEST["hash_value"];

		$checkHashStr = $version . $request_timestamp . $merchant_id . $order_id . 
		$invoice_no . $currency . $amount . $transaction_ref . $approval_code . 
		$eci . $transaction_datetime . $payment_channel . $payment_status . 
		$channel_response_code . $channel_response_desc . $masked_pan . 
		$stored_card_unique_id . $backend_invoice . $paid_channel . $paid_agent . 
		$recurring_unique_id . $user_defined_1 . $user_defined_2 . $user_defined_3 . 
		$user_defined_4 . $user_defined_5 . $browser_info . $ippPeriod . 
		$ippInterestType . $ippInterestRate . $ippMerchantAbsorbRate . $payment_scheme .
		$process_by . $sub_merchant_list;

		$this->log($checkHashStr);
		$this->log('SECRETKEY => ' . $SECRETKEY);
		$checkHash = hash_hmac('sha256', $checkHashStr, $SECRETKEY, false);
		$this->log('Hash code => ' . $checkHash);
		return $checkHash;
	}
}
