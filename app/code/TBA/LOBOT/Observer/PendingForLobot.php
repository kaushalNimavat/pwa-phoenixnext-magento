<?php
namespace TBA\LOBOT\Observer;

use \Psr\Log\LoggerInterface;
use \Magento\Sales\Model\OrderFactory;

class PendingForLobot implements \Magento\Framework\Event\ObserverInterface
{
  protected $objectManager;

  protected $logger;
  protected $_order;
  protected $resource;

  public function __construct(LoggerInterface $logger,
    \Magento\Sales\Model\OrderFactory $order) 
  {
    $this->logger = $logger;
    $this->_order = $order;

    $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $this->resource = $this->objectManager->get('Magento\Framework\App\ResourceConnection');
  }

  public function execute(\Magento\Framework\Event\Observer $observer)
  {
    $orderids = $observer->getEvent()->getOrderIds();

    $this->logger->info('PendingForLobot Start Process with ' . count($orderids));
    
    foreach($orderids as $orderId)
    {
      $loggerMessage = 'PendingForLobot Order ID Before Load is :: ' . $orderId;
      $this->logger->info($loggerMessage);

      $orderTar = $this->_order->create()->loadByIncrementId($orderId);
      //$orderTar = $this->_order->load($orderId);

      $status = $orderTar->getStatus();
      $orderId = $orderTar->getId();
      $loggerMessage = 'PendingForLobot Order ID is :: ' . $orderId . " | status is : " . $status;
      $this->logger->info($loggerMessage);

      $writeConnection = $this->resource->getConnection('core_write');
      $readConnection = $this->resource->getConnection('core_read');

      $is_delivery_on_ready = $this->objectManager->get('Magento\Checkout\Model\Session')->getData('is_delivery_on_ready');

      $tableOrderName = $this->resource->getTableName('tba_preorder_order');
      $tableOrderProductsName = $this->resource->getTableName('tba_preorder_order_products');

      $insertOrderStatement = "INSERT INTO " . $tableOrderName . " (order_id,create_at,is_delivery_on_ready,is_saved,status)" .
      "VALUES" . "(" . $orderId . ",'"  . $orderTar->getCreatedAt() . "'," . $is_delivery_on_ready . ",0,"  . 0 .");";

      $writeConnection->query($insertOrderStatement);
      $loggerMessage = 'Saved order id :: ' . $orderId;
      $this->logger->info($loggerMessage);
    }
  }
}