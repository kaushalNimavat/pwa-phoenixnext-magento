<?php

namespace TBA\LOBOT\Observer;

use \Psr\Log\LoggerInterface;
use Magento\Customer\Model\CustomerFactory;

class OrderStatusChangeObserver implements \Magento\Framework\Event\ObserverInterface
{
	/**
	 * @var \Magento\Sales\Model\Service\InvoiceService
	 */
	protected $_invoiceService;

	/**
	 * @var \Magento\Framework\DB\TransactionFactory
	 */
	protected $_transactionFactory;

	protected $logger;
	protected $lobotOrderTsvBuilder;
	protected $tbaCheckStockHelper;
	protected $resource;
	protected $objectManager;
	protected $customer;

	public function __construct(
		LoggerInterface $logger,
		\Magento\Sales\Model\Service\InvoiceService $invoiceService,
		\TBA\LOBOT\Controller\LobotHelper\TbaCheckStockHelper $tbaCheckStockHelper,
		\TBA\LOBOT\Controller\LobotHelper\LobotOrderTsvBuilder $lobotOrderTsvBuilder,
		\Magento\Framework\DB\TransactionFactory $transactionFactory,
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
		CustomerFactory $customer
	) {
		$this->logger = $logger;
		$this->customer = $customer;

		$this->_invoiceService = $invoiceService;
		$this->_transactionFactory = $transactionFactory;
		$this->lobotOrderTsvBuilder = $lobotOrderTsvBuilder;
		$this->tbaCheckStockHelper = $tbaCheckStockHelper;
		$this->productRepository = $productRepository;

		$this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$this->resource = $this->objectManager->get('Magento\Framework\App\ResourceConnection');
	}

	public function execute(\Magento\Framework\Event\Observer $observer)
	{
		$writeConnection = $this->resource->getConnection('core_write');
		$readConnection = $this->resource->getConnection('core_read');

		$order = $observer->getEvent()->getOrder();
		$orderId = $order->getId();
		$status = $order->getStatus();
		$isSaved = $this->GetIsSavedOrder($readConnection, $orderId);

		$loggerMessage = 'OrderStatusChangeObserver Order ID is :: ' . $orderId;
		$this->logger->info($loggerMessage);
		$loggerMessage = 'OrderStatusChangeObserver Order Status is :: ' . $status;
		$this->logger->info($loggerMessage);
		$loggerMessage = 'OrderStatusChangeObserver Order IsSaved is :: ' . $isSaved;
		$this->logger->info($loggerMessage);

		if ($status == "processing" && $isSaved == 0) {

			$order->setStatus("Order_Received");
			$order->save();

			$taxId = "Tax ID : 0105559143340";
			$kadokawaName = "KADOKAWA AMARIN COMPANY LIMITED";
			$kadokawaAdress1 = "378 Chaiyaphruk Road,";
			$kadokawaAdress2 = "Taling Chan, Bangkok 10170";
			$kadokawaAdress3 = "TEL. 02-434-0333";

			$is_delivery_on_ready = $this->GetQueryIsDeliveryOnReady($readConnection, $orderId);

			$this->logger->info('OrderStatusChangeObserver ---> TargetOrder id : ' . $orderId . " | is_delivery_on_ready : " . $is_delivery_on_ready);
			$customerId = $order->getCustomerId();
			if (empty($customerId)) {
				$email = $targetOrder->getCustomerEmail();
				if (!empty($email)) {
					$customer = $this->customer->create()->loadByEmail($email);
					$customerId = $customer->getId();
				}
			}

			$shippingAddressObj = $order->getShippingAddress();
			$telephone = "None";
			$clientName = "None";
			$orderDeliveryPostCode = "None";
			$taxPrice = 0;
			$grandTotal = 0;

			$this->SaveThisOrder($writeConnection, $orderId);

			if ($shippingAddressObj) {
				$shippingAddressArray = $shippingAddressObj->getData();
				$telephone = $shippingAddressArray['telephone'];
				$clientName = $shippingAddressArray['firstname'] . ' ' . $shippingAddressArray['lastname'];

				$company = isset($shippingAddressArray['company']) ? $shippingAddressArray['company'] : "";
				$address1 = ' ' . $shippingAddressArray['street'];
				$address2 = ' ' . $shippingAddressArray['city'] . ' ' . $shippingAddressArray['region'];
				$address3 = ' ' . $shippingAddressArray['postcode'];

				$address = $company . $address1 . $address2 . $address3;

				$orderDeliveryPostCode = $shippingAddressArray['postcode'];
			}

			$products = array();
			$productsForCheck = array();
			$orderItems = $order->getAllItems();

			$isOrderHasPreOrder = $this->CheckOrderHasPreOrderItem($orderItems);
			$isOrderHasPreOrder = false; //TODO This hard code for bypass PreOrder Logic.

			foreach ($orderItems as $product) {
				$productData = $this->productRepository->get($product->getSku());

				$loggerMessage = 'OrderStatusChangeObserver Try to get product id :: ' . $productData->getId() . ' ---> Name is :: ' . $productData->getName();
				$this->logger->info($loggerMessage);

				$stockStatus = $productData->getData('stock_status');
				$loggerMessage = 'OrderStatusChangeObserver Found product sku :: ' . $productData->getSku() . ' ---> Status is :: ' . $stockStatus;
				$this->logger->info($loggerMessage);

				$productsForCheck[$product->getSku()] = $product->getData('qty_ordered');

				if ($isOrderHasPreOrder) {
					$this->WorkOnProductSQL($orderId, $product, $readConnection, $writeConnection);
				} else {
					$barcode = $productData->getData('barcode');

					$loggerMessage = 'OrderStatusChangeObserver Product is ready, Tax is :: ' . $product->getTaxAmount();
					$this->logger->info($loggerMessage);

					$taxPrice += $product->getTaxAmount();
					$itemPrice = $product->getData('price');
					$itemQty = $product->getData('qty_ordered');
					$grandTotal += $itemPrice * $itemQty;

					$productDataBean = array(
						"record_indicator" => 'D',
						"item_number" => $product->getSku(),
						"product_number" => $barcode,
						"catagories" => $product->getCategoryIds(),
						"product_name" => $product->getName(),
						"price" => $itemPrice,
						"quantity" => $itemQty,
					);
					array_push($products, $productDataBean);
				}
			}

			$grandTotal += $order->getShippingAmount();
			$reward_spend = $order->getRewardsSpentAmount();
			if (empty($reward_spend))
				$reward_spend = 0;


			if (!$isOrderHasPreOrder) {
				$orderDataBean = array(
					'record_indicator' => 'H',
					"r3" => "3001",
					//"unique_number" => $orderId,
					"unique_number" => $order->getIncrementId(),
					//"order_number" => $orderId,
					"order_number" => $order->getIncrementId(),
					"order_serial_number" => $order->getIncrementId(),
					"order_date" => date('Ymd', strtotime($order->getCreatedAt())),
					"order_member_number" => $customerId,
					"order_name" => $clientName,
					"order_order_address1" => $address1,
					"order_order_address2" => $address2,
					"order_order_address3" => $address3,
					"order_delivery_post_code" => $orderDeliveryPostCode,
					"order_phone_number" => $telephone,
					"destination_name" => $clientName,
					"delivery_address" => $address,
					"delivery_address1" => $address1,
					"delivery_address2" => $address2,
					"delivery_address3" => $address3,
					"destination_phone_number" => $telephone,
					"name_of_client" => $taxId,
					"name_of_client_local" => $kadokawaName,
					"client_address1" => $kadokawaAdress1,
					"client_address2" => $kadokawaAdress2,
					"client_address3" => $kadokawaAdress3,
					"client_address1_local" => $kadokawaAdress1,
					"client_address2_local" => $kadokawaAdress2,
					"client_address3_local" => $kadokawaAdress3,
					"requestor_phone_number" => $telephone,
					"shipping_amount" => $order->getShippingAmount(),
					"reward_spend" => $reward_spend,
					"discount_amount" => $order->getDiscountAmount(),
					"order_tax" => $taxPrice,
				);

				$orderBean = array(
					"header" => $orderDataBean,
					"detials" => $products
				);

				$tsvFilePath = $this->lobotOrderTsvBuilder->OrderToLobotTsvBuilder($orderBean);
				$this->tbaCheckStockHelper->UpdateProductAfterCheckOutBySkuQty($productsForCheck);

				$loggerMessage = 'OrderStatusChangeObserver Order not contain PreOrder and send to Lobot :: ' . $orderId;
				$this->logger->info($loggerMessage);
			} else {
				$loggerMessage = 'OrderStatusChangeObserver Order contain PreOrder and Save to DB :: ' . $orderId;
				$this->logger->info($loggerMessage);
			}

			try {
				if (!$order->canInvoice()) {
					$loggerMessage = 'OrderStatusChangeObserver can not invoice';
					$this->logger->info($loggerMessage);
					return null;
				}
				if (!$order->getState() == 'new') {
					$loggerMessage = 'OrderStatusChangeObserver order is new';
					$this->logger->info($loggerMessage);
					return null;
				}

				$loggerMessage = 'OrderStatusChangeObserver order invoice processing';
				$this->logger->info($loggerMessage);

				$invoice = $this->_invoiceService->prepareInvoice($order);
				$invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
				$invoice->register();

				$transaction = $this->_transactionFactory->create()
					->addObject($invoice)
					->addObject($invoice->getOrder());

				$transaction->save();
			} catch (Exception $e) {
				$loggerMessage = 'OrderStatusChangeObserver Exception is :: ' . $e->getMessage();
				$this->logger->info($loggerMessage);
				return null;
			}
		}
		return $this;
	}

	private function CheckOrderHasPreOrderItem($orderItems)
	{
		foreach ($orderItems as $product) {
			$productData = $this->productRepository->get($product->getSku());
			$stockStatus = $productData->getData('stock_status');

			if ($stockStatus == "PreOrder") {
				$loggerMessage = 'CheckOrderHasPreOrderItem Found PreOrder product sku :: ' . $productData->getSku();
				$this->logger->info($loggerMessage);
				return true;
			}
		}
		return false;
	}

	private function WorkOnProductSQL($orderId, $product, $readConnection, $writeConnection)
	{
		$tableOrderName = $this->resource->getTableName('tba_preorder_order');
		$tableOrderProductsName = $this->resource->getTableName('tba_preorder_order_products');

		$query = "SELECT pre_order_id FROM " . $tableOrderName . " WHERE order_id = "
			. (int)$orderId . " LIMIT 1;";

		$loggerMessage = 'OrderStatusChangeObserver Try Q-RY query WorkOnProductSQL :: ' . $query;
		$this->logger->info($loggerMessage);

		$preOrderId = $readConnection->fetchOne($query);

		$insertProductStatement = "INSERT INTO " . $tableOrderProductsName . "(pre_order_id,product_id,product_sku,remain_qty)" .
			"VALUES" . "(" . $preOrderId . "," . $product->getId() . ",'" . $product->getSku() . "'," . $product->getData('qty_ordered') . ");";

		$loggerMessage = 'OrderStatusChangeObserver Try Q-RY insertProductStatement :: ' . $insertProductStatement;
		$this->logger->info($loggerMessage);

		$writeConnection->query($insertProductStatement);

		$query = "SELECT pre_order_products_id FROM " . $tableOrderProductsName . " WHERE pre_order_id = "
			. (int)$preOrderId . " LIMIT 1;";

		$loggerMessage = 'OrderStatusChangeObserver Try Q-RY query 2 :: ' . $query;
		$this->logger->info($loggerMessage);
		$preOrderProductsId = $readConnection->fetchOne($query);

		$updateOrderProductsStatement = "UPDATE " . $tableOrderName . " SET pre_order_products_id = " . $preOrderProductsId .
			" WHERE " . "(pre_order_id = " . $preOrderId . ");";

		$loggerMessage = 'OrderStatusChangeObserver Try Q-RY updateOrderProductsStatement :: ' . $updateOrderProductsStatement;
		$this->logger->info($loggerMessage);
		$writeConnection->query($updateOrderProductsStatement);
	}

	private function GetIsSavedOrder($readConnection, $orderId)
	{
		$tableOrderName = $this->resource->getTableName('tba_preorder_order');
		$query = "SELECT is_saved FROM " . $tableOrderName . " WHERE order_id = " . $orderId . ";";
		$wyg = $readConnection->fetchOne($query);

		$loggerMessage = 'OrderStatusChangeObserver Try Q-RY GetIsSavedOrder statement :: ' . $query  . ' ---> wyg is :: ' . $wyg;
		$this->logger->info($loggerMessage);

		return $wyg;
	}

	private function SaveThisOrder($writeConnection, $orderId)
	{
		$tableOrderName = $this->resource->getTableName('tba_preorder_order');

		$updateOrderProductsStatement = "UPDATE " . $tableOrderName . " SET is_saved = " . true .
			" WHERE " . "(order_id = " . $orderId . ");";

		$loggerMessage = 'OrderStatusChangeObserver Try Q-RY SaveThisOrder statement :: ' . $updateOrderProductsStatement;
		$this->logger->info($loggerMessage);
		$writeConnection->query($updateOrderProductsStatement);
	}

	private function GetQueryIsDeliveryOnReady($readConnection, $orderId)
	{
		$tableOrderName = $this->resource->getTableName('tba_preorder_order');
		$query = "SELECT is_delivery_on_ready FROM " . $tableOrderName . " WHERE order_id = " . $orderId . ";";
		$wyg = $readConnection->fetchOne($query);

		$loggerMessage = 'OrderStatusChangeObserver Try Q-RY GetQueryIsDeliveryOnReady statement :: ' . $query  . ' ---> wyg is :: ' . $wyg;
		$this->logger->info($loggerMessage);

		return $wyg;
	}
}
