<?php

namespace TBA\LOBOT\Controller\LobotSampler;
use \Psr\Log\LoggerInterface;
use \TBA\LOBOT\Controller\LobotHelper\LobotSftpHelper;
//use \TBA\LOBOT\Controller\LobotHelper\LobotConstants;


class LobotAcceptOrder 
{
	protected $logger;
	protected $sftpHelper;
    //protected $lobotConstants;
	
    public function __construct(
        //LobotConstants $lobotConstants,
        LoggerInterface $logger , 
        LobotSftpHelper $sftpHelper
        ) {
        $this->logger = $logger;
        $this->sftpHelper = $sftpHelper;
        $this->lobotConstants = $lobotConstants;
    }

    public function execute()
    {
    	$host = '192.168.2.222';
	    $port = 21;
	    $username = 'ftpuser001';
	    $password = 'ftpuser001';
	    $remoteDir = '/homes/ftpuser001/';
        $interfaceFile3Templete = '123123    123123    00000100001TEST9221-871E     101               20180110999999    9999            0000000000100PCS123123          ';

	    if (!function_exists("ssh2_connect")){
            die('Function ssh2_connect not found, you cannot use ssh2 here');
        }

        if (!$connection = ssh2_connect($host, $port)){
            die('Unable to connect');
        }

        if (!ssh2_auth_password($connection, $username, $password)){
            die('Unable to authenticate.');
        }

        if (!$stream = ssh2_sftp($connection)){
            die('Unable to create a stream.');
        }

        $sftpDir = "ssh2.sftp://{$stream}{$remoteDir}";

        if ($handle = opendir($sftpDir)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                }
            }
            closedir($handle);
        }

        $files = array();

        $loggerMessage = "LobotAcceptOrder Finding files : " . $glob_key;
        $this->logger->info($loggerMessage);

        foreach (glob($glob_key) as $filename) 
        {
            $loggerMessage = "LobotAcceptOrder Found file " . basename($filename);
            $this->logger->info($loggerMessage);
            array_push($files,basename($filename));
        }

        $interfaceFile3MockupContent = array();

        foreach ($files as $file)
        {
            $loggerMessage = "LobotAcceptOrder Copying file: $file";
            $this->logger->info($loggerMessage);
            $remote_dir = "ssh2.sftp://{$stream}{$remoteDir}/{$file}";
            if (!$remote = @fopen($remote_dir, 'r'))
            {
                $loggerMessage = "LobotAcceptOrder Unable to open remote file: $file";
                $this->logger->info($loggerMessage);
                continue;
            }

            $line = file($file);
            $orderHeader = $line[0];
            $orderHeaderData = explode("	", $orderHeader);
            array_push($interfaceFile3MockupContent,$this->GetDataTempMockupLine());
        }

        $dt = new DateTime();
        $timeInformat = $dt->format('YmdHis');
        $fileName = "BAENKJ".$timeInformat;
        //$mockupFileDir = $this->lobotConstants->LOCAL_TMP_INTERFACE_DIR . $file;
        $mockupFileDir = '/tmpLobotInterface/'.$fileName;
        $fileCreate = fopen($fileName, 'a');

        if($fileCreate)
        {
            foreach ($interfaceFile3MockupContent as $line)
            {
                fwrirt($fileCreate, $line."\r\n");
            }
        }
        else
        {
            $this->logger->info("LobotAcceptOrder Mockup File Not Create.");
        }
        fclose($fileCreate);

    }

    private function GetDataTempMockupLine()
    {
    	$mockupLine = "";
    	$dataLines = splitNewLine($interfaceFile3Templete);
        for ($i = 0; $i < count($dataLines); $i++)
        {
            $REC = substr($dataLines[$i],0,0);
            $KEY_INF = substr($dataLines[$i],0,0);
            $HCH_DPY_NO = substr($dataLines[$i], 0, 10);
            $NKA_DPY_NO = substr($dataLines[$i], 10,10);
            $NKA_DPY_MEISA_NO = substr($dataLines[$i], 20,6);
            $HCH_DPY_MEISAI = substr($dataLines[$i], 26,5);
            $SHN_COD = substr($dataLines[$i], 31,18);
            $DATA_KBN = substr($dataLines[$i], 49,3);
            $LOT_NO = substr($dataLines[$i], 52,15);
            $KOKI_NO = substr($dataLines[$i], 68,0);
            $NKA_YMD = substr($dataLines[$i], 67,8);
            $SSKI_KYK_PLT_COD = substr($dataLines[$i], 75,10);
            $PLANT = substr($dataLines[$i], 85,10);
            $HOK_BSY = substr($dataLines[$i], 89,4);
            $SYM_KGN = substr($dataLines[$i], 93,8);
            $NKA_SU = substr($dataLines[$i], 101,0);
            $NUM_NKA_SU = substr($dataLines[$i], 101,13);
            $NKA_SU_TUI = substr($dataLines[$i], 114,3);
            $NHN_DPY_NO = substr($dataLines[$i], 117,16);
            $bean  = array(
                'REC'=>$REC,
                'KEY_INF'=>$KEY_INF,
                'HCH_DPY_NO'=>$HCH_DPY_NO,
                'NKA_DPY_NO'=>$NKA_DPY_NO,
                'NKA_DPY_MEISA_NO'=>$NKA_DPY_MEISA_NO,
                'HCH_DPY_MEISAI'=>$HCH_DPY_MEISAI,
                'SHN_COD'=>$SHN_COD,$DATA_KBN,
                'LOT_NO'=>$LOT_NO,
                'KOKI_NO'=>$KOKI_NO,
                'NKA_YMD'=>$NKA_YMD,
                'SSKI_KYK_PLT_COD'=>$SSKI_KYK_PLT_COD,
                'SSKI_KYK_PLT_COD'=>$SSKI_KYK_PLT_COD,
                'PLANT'=>$PLANT,
                'HOK_BSY'=>$HOK_BSY,
                'SYM_KGN'=>$SYM_KGN,
                'NKA_SU'=>$NKA_SU,
                'NUM_NKA_SU'=>$NUM_NKA_SU,
                'NKA_SU_TUI'=>$NKA_SU_TUI,
                'NHN_DPY_NO'=>$NHN_DPY_NO);
            $bean['HCH_DPY_NO'] = $orderHeaderData[3];

            foreach ($bean as $data)
            {
                $mockupLine .= $data;
            }
            array_push($mockupLine,$bean);
        }
        return $mockupLine;
    }
}