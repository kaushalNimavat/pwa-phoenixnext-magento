<?php
namespace TBA\LOBOT\Cron;
use \Psr\Log\LoggerInterface;
use \TBA\LOBOT\Controller\LobotHelper\LobotProductInitTsvBuilder;
use \TBA\LOBOT\Controller\LobotHelper\LobotSftpHelper;

class LobotInitMasterDataOnSchedule
{
	protected $logger;
	protected $lobotProductInitTsvBuilder;
  	protected $lobotSftpHelper;
  	protected $productCollectionFactory;

	public function __construct(
		LoggerInterface $logger,
		LobotProductInitTsvBuilder $lobotProductInitTsvBuilder,
        LobotSftpHelper $lobotSftpHelper,
       \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
       \Magento\Framework\App\Action\Context $context)
	{
	   $this->logger = $logger;
       $this->lobotProductInitTsvBuilder = $lobotProductInitTsvBuilder;
       $this->productCollectionFactory = $productCollectionFactory;
       $this->lobotSftpHelper = $lobotSftpHelper;
	}

	function run()
	{
		$collection = $this->productCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->load();
	  	$filePath = $this->lobotProductInitTsvBuilder->ProductToLobotTsvBuilder($collection);
      	$this->lobotSftpHelper->PassFileToLobot($filePath);
	}
}